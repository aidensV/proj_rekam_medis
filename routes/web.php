<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::group(['middleware' => 'auth', Auth::guard('web')->user()], function () {
    Route::get('/', 'AuthUserLevelController@index');
// ADMIN OPERATOR
    // Route::resource('admin_pelaporan', 'OperatorAdminPelaporController');
    Route::resource('admin_assembling', 'OperatorAdminAssemblingController');
    Route::post('cek_sip','OperatorDataDokterController@cek_no_sip');
    Route::resource('data_dokter', 'OperatorDataDokterController');
    Route::resource('data_ruang', 'OperatorDataRuangController');
    Route::get('master_ruang', 'OperatorDataRuangController@listData')->name('master_ruang');
// ====================================END=====================================================

// ADMIN ASSEMBLING
    Route::resource('analisis_drm', 'AssemblingAnalisisController');
    Route::get('cari_no_rm', 'AssemblingAnalisisController@find_no_rm');
    Route::post('cari_no_rm/get', 'AssemblingAnalisisController@get_no_rm');
    // Route::post('cek_krs', 'AssemblingAnalisisController@cek_krs');
    Route::resource('data_pasien', 'AssemblingDataPasienController');

// data analisis DRM
    Route::get('cari_no_rm_edit', 'AssemblingDataDrm@find_no_rm_edit')->name('cari_no_rm_edit');
    Route::resource('data_analisis_drm', 'AssemblingDataDrm');
//END

//PRINT
    Route::get('print_detail_drm/cari/{awal}/{akhir}', 'AssemblingChartDrmController@print_detail_drm_cari');
    Route::get('print_detail_drm/{bln}/{thn}', 'AssemblingChartDrmController@print_detail_drm');
    Route::get('print_ruang/{bln}/{thn}/{id}', 'AssemblingChartRuanganController@print_ruang');
    Route::get('print_dokter/{bln}/{thn}/{id}', 'AssemblingChartDokterController@print_dokter');
// end

//    chart
    Route::get('data_analisis_drm_chart', 'AssemblingChartDrmController@index');
    Route::get('data_analisis_drm_chart/cari', 'AssemblingChartDrmController@chart_kelengkapan_drm_perbulan');
//    chart ruang
    Route::get('data_analisis_chart_ruang', 'AssemblingChartRuanganController@index');
    Route::get('data_analisis_chart_ruang/cari', 'AssemblingChartRuanganController@chart_kelengkapan_ruangan_pertanggal');
    Route::get('data_analisis_chart_ruang/cari/ruangan', 'AssemblingChartRuanganController@chart_kelengkapan_ruangan_perruangan');
//    chart dokter
    Route::get('data_analisis_chart_dokter', 'AssemblingChartDokterController@index');
    Route::get('data_analisis_chart_dokter/cari', 'AssemblingChartDokterController@chart_kelengkapan_dokter_pertanggal');
    Route::get('data_analisis_chart_dokter/cari/dokter', 'AssemblingChartDokterController@chart_kelengkapan_dokter_perdokter');
// end
// ====================================END=====================================================
//DAHBOARD
    Route::get('dash_all/{level}', 'DashboardController@index_dash_all')->name('dash_all');
// ====================================END=====================================================

// Change profile
    Route::get('change_user/{id}', 'ChangeProfileController@edit_profil_user');
    Route::put('update_user/{id}', 'ChangeProfileController@update_profil_user');
// ====================================END=====================================================

});
