-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 11, 2019 at 06:25 PM
-- Server version: 5.7.26-0ubuntu0.18.04.1
-- PHP Version: 7.2.19-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_rekam_medis`
--

-- --------------------------------------------------------

--
-- Table structure for table `analisis_drm`
--

CREATE TABLE `analisis_drm` (
  `no_analisa` int(11) NOT NULL,
  `no_rm` int(11) DEFAULT NULL,
  `id_ruang` int(11) DEFAULT NULL,
  `id_petugas` int(11) DEFAULT NULL,
  `tgl_analisa` date NOT NULL,
  `status_pengembalian` int(2) NOT NULL,
  `mrs` date DEFAULT NULL,
  `krs` date DEFAULT NULL,
  `id_dokter` int(11) DEFAULT NULL,
  `rek_indikasi_masuk` tinyint(1) DEFAULT '0',
  `rek_diagnosa_masuk` tinyint(1) DEFAULT '0',
  `rek_prosedur` tinyint(1) DEFAULT '0',
  `rek_pem_fisik` tinyint(1) DEFAULT '0',
  `rek_pem_penunjang` tinyint(1) DEFAULT '0',
  `rek_tindakan_pros` tinyint(1) DEFAULT '0',
  `rek_kode_icd9` tinyint(1) DEFAULT '0',
  `rek_diagnosa_akhir` tinyint(1) DEFAULT '0',
  `rek_kode_icd10` tinyint(1) DEFAULT '0',
  `rek_pem_medika` tinyint(1) DEFAULT '0',
  `rek_obat_pulang` tinyint(1) DEFAULT '0',
  `rek_kondisi_pulang` tinyint(1) DEFAULT '0',
  `rek_instruksi` tinyint(1) DEFAULT '0',
  `rek_indikasi_pulang` tinyint(1) NOT NULL DEFAULT '0',
  `rek_tgl_kontrol` tinyint(1) DEFAULT '0',
  `rek_kondisi_men` tinyint(1) DEFAULT '0',
  `rek_transport` tinyint(1) DEFAULT '0',
  `aut_nama_dokter` tinyint(1) DEFAULT '0',
  `aut_ttd_dokter` tinyint(1) DEFAULT '0',
  `cat_tanggal` tinyint(1) NOT NULL DEFAULT '0',
  `cat_baris` tinyint(1) DEFAULT '0',
  `cat_koreksi` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `analisis_drm`
--

INSERT INTO `analisis_drm` (`no_analisa`, `no_rm`, `id_ruang`, `id_petugas`, `tgl_analisa`, `status_pengembalian`, `mrs`, `krs`, `id_dokter`, `rek_indikasi_masuk`, `rek_diagnosa_masuk`, `rek_prosedur`, `rek_pem_fisik`, `rek_pem_penunjang`, `rek_tindakan_pros`, `rek_kode_icd9`, `rek_diagnosa_akhir`, `rek_kode_icd10`, `rek_pem_medika`, `rek_obat_pulang`, `rek_kondisi_pulang`, `rek_instruksi`, `rek_indikasi_pulang`, `rek_tgl_kontrol`, `rek_kondisi_men`, `rek_transport`, `aut_nama_dokter`, `aut_ttd_dokter`, `cat_tanggal`, `cat_baris`, `cat_koreksi`, `created_at`, `updated_at`) VALUES
(14, 156148, 6, 1, '2019-06-11', 0, '2019-02-12', '2019-02-18', 3, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, '2019-06-11 09:15:30', '2019-06-11 09:15:30'),
(15, 129817, 6, 1, '2019-06-11', 0, '2019-02-11', '2019-02-13', 3, 1, 1, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, '2019-06-11 09:18:25', '2019-06-11 09:18:25'),
(16, 162046, 6, 1, '2019-06-11', 0, '2019-02-18', '2019-02-24', 3, 1, 1, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2019-06-11 09:21:21', '2019-06-11 09:21:21'),
(17, 107690, 6, 1, '2019-06-11', 0, '2019-02-04', '2019-02-06', 3, 1, 1, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, '2019-06-11 09:23:49', '2019-06-11 09:23:49'),
(18, 70422, 3, 1, '2019-06-11', 0, '2019-03-13', '2019-03-14', 7, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, '2019-06-11 09:28:34', '2019-06-11 09:28:34'),
(19, 158007, 3, 1, '2019-06-11', 0, '2019-01-12', '2019-01-15', 4, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, '2019-06-11 09:30:56', '2019-06-11 09:30:56'),
(20, 117207, 3, 1, '2019-06-11', 0, '2019-02-14', '2019-02-18', 7, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, '2019-06-11 09:34:14', '2019-06-11 09:34:14'),
(21, 53673, 7, 1, '2019-06-11', 0, '2019-04-08', '2019-04-09', 9, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, '2019-06-11 09:37:19', '2019-06-11 09:37:19'),
(22, 164663, 12, 1, '2019-06-11', 0, '2019-03-25', '2019-03-28', 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, '2019-06-11 09:39:46', '2019-06-11 09:39:46'),
(23, 165319, 12, 1, '2019-06-11', 0, '2019-03-26', '2019-03-29', 3, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, '2019-06-11 09:42:20', '2019-06-11 09:42:20');

-- --------------------------------------------------------

--
-- Table structure for table `petugas`
--

CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL,
  `nama_petugas` varchar(50) NOT NULL,
  `username` varchar(10) NOT NULL,
  `password` text NOT NULL,
  `level` enum('1','2') NOT NULL,
  `remember_token` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `petugas`
--

INSERT INTO `petugas` (`id_petugas`, `nama_petugas`, `username`, `password`, `level`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin2', 'admin', '$2y$10$dYMUTQJ.b7AF.iwVhm/B6esdt8924DmXPirdrpRuuYS3v8d2R24q.', '1', 'jwVV8bHQrzAaa4Q7x8H0p2RqZR6GJmdNpsPkFCA3kUxzlqIqUAQbXZ2MHC2H', '2019-05-25 08:57:56', '2019-05-22 22:53:39'),
(3, 'petugas', 'petugas', '$2y$10$PFyff5FfI0N4RAWE.UhzI.39c/Kr7jeHV.fZOGEFZ1cYJpXR9JApq', '1', 'fBcanGqEQAxT6VO7cYnolPPO9kySFbEIGDUWHjSXt9iGnfv4zaxRG54Ld4Hd', '2019-05-25 08:56:47', '2019-05-25 08:56:00'),
(4, 'petugas', 'petugas1', '$2y$10$610D/LALv4PEfVc82Pog..JSgY1By5eTSOdFbMe/dA13jBUcXnKFO', '2', '', '2019-05-25 08:57:51', '2019-05-25 08:57:51'),
(6, 'petugas2', 'petugas3', '$2y$10$jSVEKRxO5enZzzgukVbEhuSEEAsVvm30Y/GOihSjihWJnncOvd4em', '2', '', '2019-06-11 09:55:07', '2019-06-11 09:55:07');

-- --------------------------------------------------------

--
-- Table structure for table `tb_dokter`
--

CREATE TABLE `tb_dokter` (
  `id_dokter` int(11) NOT NULL,
  `nama_dokter` varchar(50) DEFAULT NULL,
  `jenis_kelamin` enum('L','P') DEFAULT NULL,
  `spesialis` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_dokter`
--

INSERT INTO `tb_dokter` (`id_dokter`, `nama_dokter`, `jenis_kelamin`, `spesialis`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 'dr. roosnawati', 'P', 'Anak', '2019-06-10 06:31:00', '2019-06-10 06:31:00', NULL),
(4, 'dr. bayu', 'L', 'orthopaedi', '2019-06-10 06:33:02', '2019-06-10 06:33:02', NULL),
(5, 'dr. alex', 'L', 'bedah', '2019-06-10 06:33:36', '2019-06-10 06:33:36', NULL),
(6, 'dr. hadi', 'L', 'syaraf', '2019-06-10 06:34:09', '2019-06-10 06:34:09', NULL),
(7, 'dr. hanum', 'P', 'penyakit dalam', '2019-06-10 06:34:41', '2019-06-10 06:34:41', NULL),
(8, 'dr. harto', 'L', 'penyakit dalam', '2019-06-10 06:37:16', '2019-06-10 06:37:16', NULL),
(9, 'dr. lilik', 'P', 'bedah syaraf', '2019-06-10 06:37:48', '2019-06-10 06:37:48', NULL),
(10, 'Dr. Spratman2', 'P', 'Ahli Mata', '2019-06-11 09:55:27', '2019-06-11 09:56:52', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_pasien`
--

CREATE TABLE `tb_pasien` (
  `no_rm` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jenis_kelamin` enum('NULL','L','P') DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pasien`
--

INSERT INTO `tb_pasien` (`no_rm`, `nama`, `jenis_kelamin`, `tgl_lahir`, `alamat`, `created_at`, `updated_at`, `deleted_at`) VALUES
(53673, 'Hartaty Nasan', 'P', '1970-02-26', 'JL KEDURUS III PILANG MUKTI KARANGPILANG KOTA TANGERANG', '2019-06-10 06:45:48', '2019-06-10 06:45:48', NULL),
(70422, 'Dwi Ayuwulansari', 'P', '1994-07-21', 'MANUKAN LOR GANG VIIG NO.5 TANDES KOTA SURABAYA', '2019-06-10 06:46:56', '2019-06-10 06:46:56', NULL),
(107690, 'Anjani Cessa', 'P', '2008-11-28', 'JL BRIGJEN KATAMSO 3/ 29 WARU KAB. SIDOARJO', '2019-06-10 06:43:32', '2019-06-10 06:43:32', NULL),
(117207, 'Titis Dwi Suprapti', 'P', '1960-11-06', 'JL HAYAM WURUK BLOK B-08 GEDANGAN KAB. SIDOARJO', '2019-06-10 06:47:29', '2019-06-10 06:47:29', NULL),
(129817, 'Vania Keisha', 'P', '2014-10-02', 'KEBONSARI VI A/3 JAMBANGAN KOTA SURABAYA', '2019-06-10 06:42:15', '2019-06-10 06:42:15', NULL),
(156148, 'Nafiah Firzanah Z', 'P', '2013-03-04', 'ASPOL WAGE 1 NO 43 GAYUNGAN KOTA SURABAYA', '2019-06-10 06:41:42', '2019-06-10 06:41:42', NULL),
(158007, 'malik Maryono', 'L', '1967-08-08', 'KAVLING CITRA LESTARI A/16 TAMAN KAB. SIDOARJO', '2019-06-10 06:40:31', '2019-06-10 06:40:31', NULL),
(162046, 'Rodhiatus Novia A', 'P', '2009-11-09', 'JL TENTARA PELAJAR NO 173 WAJO KOTA MAKASSAR', '2019-06-10 06:42:48', '2019-06-10 06:42:48', NULL),
(164663, 'By Ny. Alfia Nurul Fathia', 'P', '2019-03-15', 'KEDURUS SAWAH GEDE 3/31 KARANGPILANG KOTA SURABAYA', '2019-06-10 06:44:04', '2019-06-10 06:44:04', NULL),
(165319, 'By. Ny. Maimanah', 'P', '2019-03-26', 'JL PAGESANGAN BLOK 67/E-03 GAYUNGAN KOTA SURABAYA', '2019-06-10 06:44:46', '2019-06-10 06:44:46', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_ruangan`
--

CREATE TABLE `tb_ruangan` (
  `id_ruang` int(25) NOT NULL,
  `nama_ruang` varchar(50) DEFAULT NULL,
  `kelas_ruang` varchar(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_ruangan`
--

INSERT INTO `tb_ruangan` (`id_ruang`, `nama_ruang`, `kelas_ruang`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Mawar', '12', '2019-05-25 22:58:53', '2019-06-10 16:36:22', '2019-06-10 16:36:22'),
(2, 'Edelweis', 'II', '2019-05-25 23:00:18', '2019-06-10 16:37:40', NULL),
(3, 'flamboyan', 'I', '2019-06-09 23:49:16', '2019-06-09 23:49:16', NULL),
(4, 'Anggrek', 'VIP', '2019-06-10 16:36:38', '2019-06-10 16:36:38', NULL),
(5, 'Teratai', 'VVIP', '2019-06-10 16:37:09', '2019-06-10 16:37:09', NULL),
(6, 'Dahlia', 'III', '2019-06-10 16:38:04', '2019-06-10 16:38:04', NULL),
(7, 'Melati', 'III', '2019-06-10 16:38:13', '2019-06-10 16:38:13', NULL),
(8, 'ICU', 'Khusus', '2019-06-10 16:38:37', '2019-06-10 16:38:37', NULL),
(9, 'Isolasi', 'Khusus', '2019-06-10 16:39:04', '2019-06-10 16:39:04', NULL),
(10, 'Perinatologi', 'Khusus', '2019-06-10 16:39:18', '2019-06-10 16:39:18', NULL),
(11, 'Tahanan', 'Khusus', '2019-06-10 16:39:31', '2019-06-10 16:39:31', NULL),
(12, 'neonatus', 'Khusus', '2019-06-10 23:01:26', '2019-06-10 23:01:26', NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_item`
-- (See below for the actual view)
--
CREATE TABLE `view_item` (
`bln` varchar(9)
,`jml_data` bigint(66)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_kel_dokter`
-- (See below for the actual view)
--
CREATE TABLE `view_kel_dokter` (
`id_dokter` int(11)
,`jml_data` bigint(66)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_kel_ruang`
-- (See below for the actual view)
--
CREATE TABLE `view_kel_ruang` (
`tgl_analisa` date
,`id_ruang` int(11)
,`jml_data` bigint(66)
);

-- --------------------------------------------------------

--
-- Structure for view `view_item`
--
DROP TABLE IF EXISTS `view_item`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_item`  AS  select concat((case month(`analisis_drm`.`tgl_analisa`) when 1 then 'Januari' when 2 then 'Februari' when 3 then 'Maret' when 4 then 'April' when 5 then 'Mei' when 6 then 'Juni' when 7 then 'Juli' when 8 then 'Agustus' when 9 then 'September' when 10 then 'Oktober' when 11 then 'November' when 12 then 'Desember' end)) AS `bln`,((case when isnull(`tb_pasien`.`alamat`) then 0 when isnull(`tb_pasien`.`nama`) then 0 when isnull(`tb_pasien`.`jenis_kelamin`) then 0 when isnull(`tb_pasien`.`tgl_lahir`) then 0 else 1 end) * (((((((((((((((((((((`analisis_drm`.`rek_indikasi_masuk` * `analisis_drm`.`rek_diagnosa_masuk`) * `analisis_drm`.`rek_prosedur`) * `analisis_drm`.`rek_pem_fisik`) * `analisis_drm`.`rek_pem_penunjang`) * `analisis_drm`.`rek_tindakan_pros`) * `analisis_drm`.`rek_kode_icd9`) * `analisis_drm`.`rek_diagnosa_akhir`) * `analisis_drm`.`rek_kode_icd10`) * `analisis_drm`.`rek_pem_medika`) * `analisis_drm`.`rek_obat_pulang`) * `analisis_drm`.`rek_kondisi_pulang`) * `analisis_drm`.`rek_instruksi`) * `analisis_drm`.`rek_indikasi_pulang`) * `analisis_drm`.`rek_tgl_kontrol`) * `analisis_drm`.`rek_kondisi_men`) * `analisis_drm`.`rek_transport`) * `analisis_drm`.`aut_nama_dokter`) * `analisis_drm`.`aut_ttd_dokter`) * `analisis_drm`.`cat_tanggal`) * `analisis_drm`.`cat_baris`) * `analisis_drm`.`cat_koreksi`)) AS `jml_data` from (`analisis_drm` join `tb_pasien` on((`analisis_drm`.`no_rm` = `tb_pasien`.`no_rm`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_kel_dokter`
--
DROP TABLE IF EXISTS `view_kel_dokter`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_kel_dokter`  AS  select `analisis_drm`.`id_dokter` AS `id_dokter`,((case when isnull(`tb_pasien`.`alamat`) then 0 when isnull(`tb_pasien`.`nama`) then 0 when isnull(`tb_pasien`.`jenis_kelamin`) then 0 when isnull(`tb_pasien`.`tgl_lahir`) then 0 else 1 end) * (((((((((((((((((((((`analisis_drm`.`rek_indikasi_masuk` * `analisis_drm`.`rek_diagnosa_masuk`) * `analisis_drm`.`rek_prosedur`) * `analisis_drm`.`rek_pem_fisik`) * `analisis_drm`.`rek_pem_penunjang`) * `analisis_drm`.`rek_tindakan_pros`) * `analisis_drm`.`rek_kode_icd9`) * `analisis_drm`.`rek_diagnosa_akhir`) * `analisis_drm`.`rek_kode_icd10`) * `analisis_drm`.`rek_pem_medika`) * `analisis_drm`.`rek_obat_pulang`) * `analisis_drm`.`rek_kondisi_pulang`) * `analisis_drm`.`rek_instruksi`) * `analisis_drm`.`rek_indikasi_pulang`) * `analisis_drm`.`rek_tgl_kontrol`) * `analisis_drm`.`rek_kondisi_men`) * `analisis_drm`.`rek_transport`) * `analisis_drm`.`aut_nama_dokter`) * `analisis_drm`.`aut_ttd_dokter`) * `analisis_drm`.`cat_tanggal`) * `analisis_drm`.`cat_baris`) * `analisis_drm`.`cat_koreksi`)) AS `jml_data` from (`analisis_drm` join `tb_pasien` on((`analisis_drm`.`no_rm` = `tb_pasien`.`no_rm`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_kel_ruang`
--
DROP TABLE IF EXISTS `view_kel_ruang`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_kel_ruang`  AS  select `analisis_drm`.`tgl_analisa` AS `tgl_analisa`,`analisis_drm`.`id_ruang` AS `id_ruang`,((case when isnull(`tb_pasien`.`alamat`) then 0 when isnull(`tb_pasien`.`nama`) then 0 when isnull(`tb_pasien`.`jenis_kelamin`) then 0 when isnull(`tb_pasien`.`tgl_lahir`) then 0 else 1 end) * (((((((((((((((((((((`analisis_drm`.`rek_indikasi_masuk` * `analisis_drm`.`rek_diagnosa_masuk`) * `analisis_drm`.`rek_prosedur`) * `analisis_drm`.`rek_pem_fisik`) * `analisis_drm`.`rek_pem_penunjang`) * `analisis_drm`.`rek_tindakan_pros`) * `analisis_drm`.`rek_kode_icd9`) * `analisis_drm`.`rek_diagnosa_akhir`) * `analisis_drm`.`rek_kode_icd10`) * `analisis_drm`.`rek_pem_medika`) * `analisis_drm`.`rek_obat_pulang`) * `analisis_drm`.`rek_kondisi_pulang`) * `analisis_drm`.`rek_instruksi`) * `analisis_drm`.`rek_indikasi_pulang`) * `analisis_drm`.`rek_tgl_kontrol`) * `analisis_drm`.`rek_kondisi_men`) * `analisis_drm`.`rek_transport`) * `analisis_drm`.`aut_nama_dokter`) * `analisis_drm`.`aut_ttd_dokter`) * `analisis_drm`.`cat_tanggal`) * `analisis_drm`.`cat_baris`) * `analisis_drm`.`cat_koreksi`)) AS `jml_data` from (`analisis_drm` join `tb_pasien` on((`analisis_drm`.`no_rm` = `tb_pasien`.`no_rm`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `analisis_drm`
--
ALTER TABLE `analisis_drm`
  ADD PRIMARY KEY (`no_analisa`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`id_petugas`);

--
-- Indexes for table `tb_dokter`
--
ALTER TABLE `tb_dokter`
  ADD PRIMARY KEY (`id_dokter`);

--
-- Indexes for table `tb_pasien`
--
ALTER TABLE `tb_pasien`
  ADD PRIMARY KEY (`no_rm`);

--
-- Indexes for table `tb_ruangan`
--
ALTER TABLE `tb_ruangan`
  ADD PRIMARY KEY (`id_ruang`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `analisis_drm`
--
ALTER TABLE `analisis_drm`
  MODIFY `no_analisa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `petugas`
--
ALTER TABLE `petugas`
  MODIFY `id_petugas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_dokter`
--
ALTER TABLE `tb_dokter`
  MODIFY `id_dokter` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tb_ruangan`
--
ALTER TABLE `tb_ruangan`
  MODIFY `id_ruang` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
