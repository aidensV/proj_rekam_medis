<?php

use Faker\Generator as Faker;

$factory->define(App\analisa_drm::class, function (Faker $faker) {
    return [

      'analisis_no' => $faker->randomNumber(5),
      'pasien_no_rm' => $faker->randomNumber(6),
      'ruang_id' => $faker->randomNumber(3),
      'analisis_lama_dirawat' => $faker->randomNumber(3),
      'analisis_tgl' => $faker->date,
      'analisis_mrs' => $faker->date,
      'analisis_krs' => $faker->date

    ];
});
