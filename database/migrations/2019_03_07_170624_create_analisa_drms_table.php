<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnalisaDrmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('analisa_drms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('analisis_no');
            $table->integer('pasien_no_rm');
            $table->integer('ruang_id');
            $table->integer('analisis_lama_dirawat');
            $table->date('analisis_tgl');
            $table->date('analisis_mrs');
            $table->date('analisis_krs');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('analisa_drms');
    }
}
