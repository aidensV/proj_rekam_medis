<?php $hal = 'dashboard' ?>
<?php $sub = 0 ?>
@extends('layouts.admin_assembling.master')
@section('title','Admin  - Profile ')

@section('content')




<div class="container-fluid">
    <div class="row page-titles">
        <div class="col p-0">
            <h4>Hello, {{ Auth::user()->name }} <span>Welcome here</span></h4>
        </div>
        <div class="col p-0">

        </div>
    </div>
    <div class="col-xl-10">
                       <div class="card forms-card">
                           <div class="card-body">
                               <h4 class="card-title mb-4">Ubah Profile</h4>
                               <div class="basic-form">

                                   <form  action="{{url('update_user')}}/{{$user->id_petugas}}" method="post">
                                     {{ csrf_field() }}
                                     {{ method_field('PUT') }}

                                       <div class="form-group">
                                           <label class="text-label">Username*</label>
                                           <input type="text" readonly name="username" class="form-control" value="{{$user->username}}" required>
                                       </div>
                                       <div class="form-group">
                                           <label class="text-label">Password Baru*</label>
                                           <input type="password" name="password" class="form-control" placeholder="************" >
                                       </div>
                                       <div class="form-group">
                                           <label class="text-label">Nama*</label>
                                           <input type="text" name="nama" value="{{$user->nama_petugas}}" class="form-control" placeholder="Dr.Jhony" required>
                                       </div>


                                       <button type="submit" class="btn btn-primary btn-form mr-2">Simpan</button>
                                       @if (Auth::user()->level == '1')
                                           <a href="{{route('dash_all', [1])}}" class="btn btn-light text-dark btn-form">Cancel</a>
                                           @elseif(Auth::user()->level == '2')
                                           <a href="{{route('dash_all', [2])}}" class="btn btn-light text-dark btn-form">Cancel</a>
                                       @endif
                                       {{-- <button type="button" class="btn btn-light text-dark btn-form">Cancel</button> --}}
                                   </form>

                               </div>
                           </div>
                       </div>
                   </div>


</div>
@endsection
