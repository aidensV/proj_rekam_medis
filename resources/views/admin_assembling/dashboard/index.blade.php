<?php $hal = 'dashboard' ?>
<?php $sub = 0 ?>
@extends('layouts.admin_assembling.master')
@section('title','Admin Dashboard - Dashboard Assembling')

@section('content')




<div class="container-fluid">
  <div class="row page-titles">
    <div class="col p-0">
      <h4>Hello, <span>Welcome here</span></h4>
    </div>
    <div class="col p-0">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0)">Layout</a>
        </li>
        <li class="breadcrumb-item active">Blank</li>
      </ol>
    </div>
  </div>
  <div class="row">
    <div class="col-xl-3 col-sm-6 col-xxl-6">
      <div class="card sale-widget">
        <div class="card-body gradient-7">
          <h4 class="card-title text-white text-uppercase">Ruangan Paling Banyak Tidak Lengkap</h4>
          <h2 class="text-white price-tag"> <span>"{{$ruangan_tl}}"</span></h2>

        </div>
      </div>

      <div class="card sale-widget">
        <div class="card-body gradient-1 rounded stat-widget-seven">
          <div class="media pl-xl-8 align-items-center">
            <img class="mr-7 mt-7" src="{{asset('public/assets/images/icons/project-complete.png')}}" alt="">
            <div class="media-body pl-9">
              <h2 class="mt-0 mb-9">{{round($chart_l,1)}} {{'%'}}</h2>
              <h6 class="text-uppercase text-white">Jumlah Drm Lengkap</h6>
              <h6 class="text-uppercase text-white">Per : {{date('M',strtotime($bln))}} - {{date('Y',strtotime($thn))}}</h6>

            </div>
          </div>
        </div>
      </div>

    </div>
    <div class="col-xl-3 col-sm-6 col-xxl-6">
      <div class="card sale-widget">
        <div class="card-body gradient-2">
          <h4 class="card-title text-white text-uppercase">Item Paling Tidak Lengkap</h4>
          <h2 class="text-white price-tag"> <span>"{{$item_tl}}"</span></h2>

        </div>
      </div>

      <div class="card sale-widget">
        <div class="card-body gradient-5 rounded stat-widget-seven">
          <div class="media pl-xl-8 align-items-center">
            <img class="mr-7 mt-7" src="{{asset('public/assets/images/icons/project-complete.png')}}" alt="">
            <div class="media-body pl-9">
              <h2 class="mt-0 mb-9">{{round($chart_tl,1)}} {{'%'}}</h2>
              <h6 class="text-uppercase text-white">Jumlah Drm Tidak Lengkap</h6>
              <h6 class="text-uppercase text-white">Per : {{date('M',strtotime($bln))}} - {{date('Y',strtotime($thn))}}</h6>
            </div>
          </div>
        </div>
      </div>

    </div>

  </div>
</div>
@endsection
