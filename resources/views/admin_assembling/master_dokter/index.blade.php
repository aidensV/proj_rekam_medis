<?php $hal = 'data_dokter' ?>
<?php $sub = 0 ?>
@extends('layouts.admin_assembling.master')
@section('content')


<div class="container-fluid">
  <div class="row page-titles">
    <div class="col p-0">
      <h4>Hello, {{ Auth::user()->name }} <span>Welcome back</span></h4>
    </div>
    <div class="col p-0">
    </div>
  </div>
  <a href="{{route('data_dokter.create')}}" style="margin-left:15px;margin-bottom:10px;" class="btn btn-primary btn-ft">Tambah Data</a>


  <div class="col-12">
    <div class="card">
      <div class="card-body">
        <div class="table-responsive">
          <table id="example" class="display" style="min-width: 845px">
            <thead>
              <tr>
                <th>No</th>
                <th>Id Dokter</th>
                <th>Nama</th>
                <th>Spesialis</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($data_dokter as $data)
              <tr>
                <td>{{$no++}}</td>
                <td>{{$data->id_dokter}}</td>
                <td>{{$data->nama_dokter}}</td>
                <td>{{$data->spesialis}}</td>
                <td>
                  <form action="{{ route('data_dokter.destroy', $data->id_dokter) }}" method="post">
                    {{csrf_field()}}
                    {{ method_field('DELETE') }}
                    {{-- <a href="{{route('owner_marketing.show',$data->id_produk)}}" style="margin-right:10px;" class="btn btn-primary" data-toggle="tooltip" data-placement="botttom" title="Lihat Detail"><i class="fa fa-eye"></i></a> --}}
                    <a href="{{route('data_dokter.edit',$data->id_dokter)}}" class="btn btn-warning btn-ft" data-toggle="tooltip" data-placement="botttom" title="Edit Data"><i class="icon ion-edit"></i></a>
                    <button class="btn btn-danger btn-ft" onclick="return confirm('Apakah Anda yakin data akan dihapus?');" data-toggle="tooltip" data-placement="botttom" title="Hapus Data"><i class="icon ion-trash-b"></i></button>

                  </form>

                </td>
              </tr>
              @endforeach
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
