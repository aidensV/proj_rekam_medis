<?php $hal = 'data_dokter' ?>
<?php $sub = 0 ?>
@extends('layouts.admin_assembling.master')
@section('content')

<div class="container-fluid">
  <div class="row page-titles">
    <div class="col p-0">
      <h4>Hello, {{ Auth::user()->nama_petugas }} <span>Welcome here</span></h4>
    </div>
    <div class="col p-0">

    </div>
  </div>
  <div class="col-xl-10">
    <div class="card forms-card">
      <div class="card-body">
        <h4 class="card-title mb-4">Tambah Data Dokter</h4>
        <div class="basic-form">
          <form action="{{route('data_dokter.store')}}" method="post">
            {{ csrf_field() }}

            {{-- <div class="form-group">
              <label class="text-label">Id D*</label>
              <input type="number"  name="dokter_sip" id="dokter_sip" class="form-control" placeholder="1726xxxxx" required>
            </div> --}}
            <div class="form-group">
              <label class="text-label">Nama Dokter*</label>
              <input type="text" name="dokter_nama" class="form-control" placeholder="Dr.Jhony" required>
            </div>
            <div class="form-group">
              <label class="text-label">Jenis kelamin*</label>
              <div class="col-sm-6 col-md-4 col-xl">
                <div class="form-radio">

                  <input id="radio15" class="" name="dokter_jk" value="L" type="radio">
                  <label for="radio15" class="radio-green">Laki-Laki</label>

                  <input id="radio14" class="" name="dokter_jk" value="P" type="radio">
                  <label for="radio14" class="radio-green">Perempuan</label>

                </div>
              </div>
            </div>

            <div class="form-group">
              <label class="text-label">Spesialis*</label>
              <input type="text" name="dokter_spesialis" class="form-control" placeholder="Ahli Mata" required>
            </div>

            <button type="submit" class="btn btn-primary btn-form mr-2">Simpan</button>
            <a href="{{url('data_dokter')}}" class="btn btn-light text-dark btn-form">Batal</a>
            {{-- <button type="button" class="btn btn-light text-dark btn-form">Cancel</button> --}}
          </form>
        </div>
      </div>
    </div>
  </div>


</div>
@endsection
@section('script')
  {{-- <script>

  $(document).ready(function(){
    $('#dokter_id').keyup(function(){

    })
  })
  </script> --}}
@endsection
