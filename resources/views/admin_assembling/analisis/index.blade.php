<?php $hal = 'analisis_drm' ?>
<?php $sub = 0 ?>
@extends('layouts.admin_assembling.master')
@section('title','Assembling - Analisa DRM')
@section('css')
<link href="{{asset('public/assets/plugins/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">
@endsection
@section('content')




<div class="container-fluid">
  <div class="row page-titles">
    <div class="col p-0">
      <h4>Hello, <span>Welcome here</span></h4>
    </div>
    <div class="col p-0">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0)">Layout</a>
        </li>
        <li class="breadcrumb-item active">Blank</li>
      </ol>
    </div>
  </div>
  {{-- <form action="index.html" method="post"> --}}
  <div class="row">

    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title mb-5">Analisis Resume Medis</h4>
          <form action="{{route('analisis_drm.store')}}" method="post" id="step-form-horizontal" class="step-form-horizontal">
              {{csrf_field()}}
          <div>
              <h4>Data Kelengkapan</h4>
              <section>
                  <div class="row">
                      <div class="col-lg-12 mb-2">
                          <div class="form-group">
                              <label class="text-label">Nama Dokter*</label>
                              <select name="dokter_id" class="single-select-placeholder js-states form-control">
                                  @foreach ($dokter as $data_d)
                                      <option value="{{$data_d->id_dokter}}">{{$data_d->nama_dokter}}</option>
                                  @endforeach
                              </select>
                          </div>
                      </div>
                      <div class="col-lg-12 mb-2">
                          <div class="form-group">
                              <label class="text-label">Nama Ruangan*</label>
                              <select name="ruang_id" class="single-select-placeholder js-states form-control">
                                  @foreach ($ruangan as $data_r)
                                      <option value="{{$data_r->id_ruang}}">{{$data_r->nama_ruang}}</option>
                                  @endforeach
                              </select>
                          </div>
                      </div>


                      <div class="col-lg-12 mb-2">
                          <div class="form-group">
                              <label class="text-label">Tanggal Masuk*</label>
                              <input type="text" name="analisis_mrs" autocomplete="off" class="form-control mydatepicker" data-date-format="dd-mm-yyyy" placeholder="tanggal-bulan-tahun" >
                          </div>
                      </div>
                      <div class="col-lg-12 mb-2">
                          <div class="form-group">
                              <label class="text-label">Tanggal Keluar*</label>
                              <input type="text" name="analisis_krs" id="analisis_krs" onchange="btsTgl(this.value)" autocomplete="off" class="form-control mydatepicker" data-date-format="dd-mm-yyyy" placeholder="tanggal-bulan-tahun" >
                          </div>

                      </div>
                      <div class="col-lg-12 mb-2">
                          <div class="form-group">
                              <p id="bts_tgl" style="font-size:16px;font-weight:bold; color:red;"></p>
                              <input type="hidden" name="tgl_cocok" id="tgl_cocok" autocomplete="off"  placeholder="tanggal-bulan-tahun" >
                              {{-- <input type="text" name="analisis_krs" id="analisis_krs" onchange="btsTgl" autocomplete="off" class="form-control mydatepicker" data-date-format="dd/mm/yyyy" placeholder="tanggal/bulan/tahun" > --}}
                          </div>

                      </div>
                  </div>
              </section>
              <h4>Review Identifikasi</h4>
              <section>
                  <div class="row">
                      <div class="col-lg-12 mb-2">
                          <div class="form-group">
                              <label class="text-label">Nama Pasien*</label>
                              <select id="cari_no_rm" style="width:100%!important;" class="js-data-example-ajax w-100 cari_rm "></select>
                              <input type="hidden" name="pasien_nama" id="pasien_nama" readonly class="form-control" >
                          </div>
                      </div>

                      <div class="col-lg-6 mb-2">
                          <div class="form-group">
                              <label class="text-label">No.Rekam Medis</label>
                              <input type="text" id="no_rm" name="pasien_no_rm" id="pasien_nama" readonly class="form-control" >
                          </div>
                      </div>

                      <div class="col-lg-6 mb-2">
                          <div class="form-group">
                              <label class="text-label">Tanggal Lahir</label>
                              <input type="text" name="pasien_tgl_lahir" id="pasien_tgl_lahir" readonly class="form-control" >
                          </div>
                      </div>
                      <div class="col-lg-6 mb-2">
                          <div class="form-group">
                              <label class="text-label">Jenis Kelamin</label>
                              <input type="text" name="pasien_jk" id="pasien_jk" readonly class="form-control" >
                          </div>
                      </div>
                      <div class="col-lg-12 mb-2">
                          <div class="form-group">
                              <label class="text-label">Alamat</label>
                              <input type="text" name="pasien_alamat" id="pasien_alamat" readonly class="form-control" >
                          </div>
                      </div>
                      <div class="col-lg-12 mb-2">
                          <div class="form-group">
                            <input type="hidden" name="analis_status_peng" id="analis_status_peng" readonly class="form-control" >
                              <p id="analis_status" style="font-size:16px;font-weight:bold; color:red;"></p>
                          </div>
                      </div>

                  </div>
              </section>
              <h4>Review Pelaporan</h4>
              <section>
                  <div class="row">
                      <div class="col-lg-4 mb-4">
                          <div class="form-group">
                              <input id="cb_indi_masuk" name="cb_indi_masuk" value="1" class="form-check-input styled-checkbox" type="checkbox">
                              <label for="cb_indi_masuk" class="form-check-label check-green">Indikasi Masuk</label>
                          </div>
                      </div>
                      <div class="col-lg-4 mb-4">
                          <div class="form-group">
                              <input id="cb_diag_masuk" name="cb_diag_masuk" value="1" class="form-check-input styled-checkbox" type="checkbox">
                              <label for="cb_diag_masuk" class="form-check-label check-green">Diagnosa Masuk</label>
                          </div>
                      </div>
                      <div class="col-lg-4 mb-4">
                          <div class="form-group">
                              <input id="cb_tindakan" name="cb_tindakan" value="1" class="form-check-input styled-checkbox" type="checkbox">
                              <label for="cb_tindakan" class="form-check-label check-green">Prosedur Diagnosa</label>
                          </div>
                      </div>
                      <div class="col-lg-4 mb-4">
                          <div class="form-group">
                              <input id="cb_pem_fisik" name="cb_pem_fisik" value="1" class="form-check-input styled-checkbox" type="checkbox">
                              <label for="cb_pem_fisik" class="form-check-label check-green">Pemeriksaan Fisik</label>
                          </div>
                      </div>
                      <div class="col-lg-4 mb-4">
                          <div class="form-group">
                              <input id="cb_pem_penunjang" name="cb_pem_penunjang" value="1" class="form-check-input styled-checkbox" type="checkbox">
                              <label for="cb_pem_penunjang" class="form-check-label check-green">Pemeriksaan Penunjang</label>
                          </div>
                      </div>
                      <div class="col-lg-4 mb-4">
                          <div class="form-group">
                              <input id="cb_tindakan_pros" name="cb_tindakan_pros" value="1" class="form-check-input styled-checkbox" type="checkbox">
                              <label for="cb_tindakan_pros" class="form-check-label check-green">Tindakan & Prosedur</label>
                          </div>
                      </div>

                      <div class="col-lg-4 mb-4">
                          <div class="form-group">
                              <input id="cb_icd9" name="cb_icd9" value="1" class="form-check-input styled-checkbox" type="checkbox">
                              <label for="cb_icd9" class="form-check-label check-green">ICD-9</label>
                          </div>
                      </div>

                      <div class="col-lg-4 mb-4">
                          <div class="form-group">
                              <input id="cb_diag_akhir" name="cb_diag_akhir" value="1" class="form-check-input styled-checkbox" type="checkbox">
                              <label for="cb_diag_akhir" class="form-check-label check-green">Diagnosa Akhir</label>
                          </div>
                      </div>
                      <div class="col-lg-4 mb-4">
                          <div class="form-group">
                              <input id="cb_icd10" name="cb_icd10" value="1" class="form-check-input styled-checkbox" type="checkbox">
                              <label for="cb_icd10" class="form-check-label check-green">ICD10</label>
                          </div>
                      </div>
                      <div class="col-lg-4 mb-4">
                          <div class="form-group">
                              <input id="cb_pem_medik" name="cb_pem_medik" value="1" class="form-check-input styled-checkbox" type="checkbox">
                              <label for="cb_pem_medik" class="form-check-label check-green">Pemberian Medikamentosa</label>
                          </div>
                      </div>
                      <div class="col-lg-4 mb-4">
                          <div class="form-group">
                              <input id="cb_obt_pulang" name="cb_obt_pulang" value="1" class="form-check-input styled-checkbox" type="checkbox">
                              <label for="cb_obt_pulang" class="form-check-label check-green">Obat Pulang</label>
                          </div>
                      </div>
                      <div class="col-lg-4 mb-4">
                          <div class="form-group">
                              <input id="cb_kond_pulang" name="cb_kond_pulang" value="1" class="form-check-input styled-checkbox" type="checkbox">
                              <label for="cb_kond_pulang" class="form-check-label check-green">Kondisi Pulang</label>
                          </div>
                      </div>

                      <div class="col-lg-4 mb-4">
                          <div class="form-group">
                              <input id="cb_instruksi" name="cb_instruksi" value="1" class="form-check-input styled-checkbox" type="checkbox">
                              <label for="cb_instruksi" class="form-check-label check-green">Instruksi Lanjut</label>
                          </div>
                      </div>
                      <div class="col-lg-4 mb-2">
                          <div class="form-group">
                              <input id="cb_indi_pulang" name="cb_indi_pulang" value="1" class="form-check-input styled-checkbox" type="checkbox">
                              <label for="cb_indi_pulang" class="form-check-label check-green">Indikasi Pulang</label>
                          </div>
                      </div>
                      <div class="col-lg-4 mb-4">
                          <div class="form-group">
                              <input id="cb_tgl_kontrol" name="cb_tgl_kontrol" value="1" class="form-check-input styled-checkbox"  type="checkbox">
                              <label for="cb_tgl_kontrol" class="form-check-label check-green">Tanggal Kontrol</label>
                          </div>
                      </div>
                      <div class="col-lg-4 mb-4">
                          <div class="form-group">
                              <input id="cb_kond_mendesak" name="cb_kond_mendesak" value="1" class="form-check-input styled-checkbox" type="checkbox">
                              <label for="cb_kond_mendesak" class="form-check-label check-green">Kondisi Mendesak</label>
                          </div>
                      </div>
                      <div class="col-lg-4 mb-4">
                          <div class="form-group">
                              <input id="cb_alat_transp" name="cb_alat_transp" value="1" class="form-check-input styled-checkbox" type="checkbox">
                              <label for="cb_alat_transp" class="form-check-label check-green">Alat Transportasi</label>
                          </div>
                      </div>
                  </div>
              </section>
              <h4>Review Auth & Pencatatan</h4>
              <section>
                  <div class="row">
                      <div class="col-lg-12 mb-4">
                          <div class="form-group">
                              <label class="text-label">
                                  <h3>Review Authentifikasi</h3>
                              </label>
                          </div>
                      </div>
                      <div class="col-lg-4 mb-4">
                          <div class="form-group">
                              <input id="cb_ttd_dokter" name="cb_ttd_dokter" value="1" class="form-check-input styled-checkbox" type="checkbox">
                              <label for="cb_ttd_dokter" class="form-check-label check-green">TTD Dokter</label>
                          </div>
                      </div>
                      <div class="col-lg-4 mb-4">
                          <div class="form-group">
                              <input id="cb_nama_dokter" name="cb_nama_dokter" value="1" class="form-check-input styled-checkbox" type="checkbox">
                              <label for="cb_nama_dokter" class="form-check-label check-green">Nama Dokter</label>
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-lg-12 mb-4">
                          <div class="form-group">
                              <label class="text-label">
                                  <h3>Review Pencatatan</h3>
                              </label>
                          </div>
                      </div>
                      <div class="col-lg-4 mb-4">
                          <div class="form-group">
                              <input id="cb_baris" name="cb_baris" value="1" class="form-check-input styled-checkbox" type="checkbox">
                              <label for="cb_baris" class="form-check-label check-green">Baris</label>
                          </div>
                      </div>
                      <div class="col-lg-4 mb-4">
                          <div class="form-group">
                              <input id="cb_koreksi" name="cb_koreksi" value="1" class="form-check-input styled-checkbox" type="checkbox">
                              <label for="cb_koreksi" class="form-check-label check-green">Koreksi</label>
                          </div>
                      </div>
                      <div class="col-lg-4 mb-4">
                          <div class="form-group">
                              <input id="cb_tgl" name="cb_tgl" value="1" class="form-check-input styled-checkbox" type="checkbox">
                              <label for="cb_tgl" class="form-check-label check-green">Tanggal Pelayanan</label>
                          </div>
                      </div>
                  </div>

              </section>

          </div>
          </form>

        </div>
      </div>
    </div>

  </div>


</div>
@endsection
@section('js')
<script src="{{asset('public/assets/plugins/select2/js/select2.full.min.js')}}"></script>
<script src="{{asset('public/assets/js/plugins-init/select2-init.js')}}"></script>
<script type="text/javascript">
  // Cari Nomor Rekam Medis Dan Nama Pasien
  $('.cari_rm').select2({
    placeholder: 'Cari dengan No Rekam Medis atau Nama Pasien',
    ajax: {
      url: 'cari_no_rm',
      dataType: 'json',
      delay: 250,
      processResults: function(data) {
        return {
          results: $.map(data, function(item) {
            return {
              text: item.nama,
              id: item.no_rm
            }
          })
        };
      },

      cache: true
    }
  });
  // getNama(id)
  $('#cari_no_rm').on('change', function(e) {
    $("#no_rm").val($(this).find('option:selected').val())
    // $("#no_rm").val($(this).val())
    var no_rm = $(this).val();

    $.ajax({
        url     : 'cari_no_rm/get',
        type    : 'POST',
        dataType: "json",
        data    : { "_token": "{{ csrf_token() }}",'no_rm':no_rm},

        timeout : 30000,
        success : function(e) {

            if(e==0){ //Show error if data not found.
                $('#error').html('Data not found');
                $('#pasien_nama').val('');
                $('#pasien_nama_ibu').val('');
                $('#pasien_tgl_lahir').val('');
                $('#pasien_jk').val('');
                $('#pasien_alamat').val('');

            }
            else {//assign value to each input by json
                $('#error').html('');

                // r = $.parseJSON(e); //convert json to array
                if (e.jenis_kelamin == 'P'){
                    var jk = 'Perempuan';
                } else {
                    var jk = 'Laki-Laki';
                }

                var tgl = moment(e.tgl_lahir).locale('id').format("DD-MMMM-YYYY");
                if (window.console) {
                  // console.log(tgl);
                }
                $('#pasien_nama').val(e.nama); //assign name value
                $('#pasien_tgl_lahir').val(tgl);
                $('#pasien_jk').val(jk);
                $('#pasien_alamat').val(e.alamat);
            }

        }

    });
    // AJAX CEK KRS
    $.ajax({
        url     : 'cek_krs',
        type    : 'POST',
        dataType: "json",
        data    : { "_token": "{{ csrf_token() }}",'no_rm':no_rm},

        timeout : 30000,
        success : function(e) {

            if(e==0){ //Show error if data not found.
                $('#error').html('Data not found');
                $('#analisis_status').val('');


            }
            else {//assign value to each input by json
                $('#error').html('');

                // r = $.parseJSON(e); //convert json to array

                //
                // var tgl_sekarang = moment().locale('id').format('YYYY-MM-DD H:mm:ss');
                // var tgl_kembali = moment(e.analis_krs).locale('id').add(2,'d').format('YYYY-MM-DD H:mm:ss');
                // // $('#analis_status').val(tgl); //assign name value
                // if (e.analis_krs === undefined) {
                //   document.getElementById("analis_status").innerHTML = "";
                //   $('#analis_status_peng').val("1"); //assign name value
                //
                // }
                // else{
                //   if (tgl_sekarang >= tgl_kembali) {
                //     document.getElementById("analis_status").innerHTML = "Terlambat Mengembalikan ";
                //     $('#analis_status_peng').val("3"); //assign name value
                //
                //   }else {
                //     document.getElementById("analis_status").innerHTML = "";
                //     $('#analis_status_peng').val("2"); //assign name value
                //   }
                // }

                if (window.console) {
                  console.log(e.analis_krs);
                  console.log(tgl_sekarang);
                  console.log(tgl_kembali);
                }

            }

        }

    });

  });
//   function btsTgl(val) {
//     // var tgl = moment(val).locale("id").format("DD-MMMM-YYYY");
//     // var tgl = moment("15-04-2019").locale("id").format("DD-MMMM-YYYY");
//   var tgl_kembali=  moment(val, "DD-MM-YYYY").add(2,'day').format('DD-MMMM-YYYY');
//   var tgl_sekarang = moment().locale('id').format('DD-MM-YYYY');
//   console.log('tanggal kembali'+val);
//   console.log('tanggal sekarang'+tgl_sekarang);
//     if (val) {
//
//       $('#tgl_cocok').val(tgl_kembali); //assign name value
//
//       document.getElementById("bts_tgl").innerHTML = "Batas Tanggal Kembali: " + tgl_kembali;
//       // document.getElementById("tgl_cocok").value(tgl_kembali);
//     }else{
//       document.getElementById("bts_tgl").innerHTML = "";
//     }
// }

</script>

@endsection
