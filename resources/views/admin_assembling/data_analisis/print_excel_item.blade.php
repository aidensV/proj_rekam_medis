
<html>
@if ($awal && $akhir == !null)
<tr>
  <th colspan="20"><h2 style="text-align:center; margin-left:50%;"> LAPORAN ANALISA KUANTITATIF LEMBAR RESUME MEDIS</h2></th>
</tr>
<tr>
  <td colspan="20"><h2>PERIODE: {{date('d-M-Y',strtotime($awal))}} s/d {{date('d-M-Y',strtotime($akhir))}}</h2></td>
</tr>
@else
  <tr style="margin:0;">
  <th colspan="20"><h2 style="text-align:center; margin-bottom: 0; margin-left:20%;"> LAPORAN ANALISA KUANTITATIF LEMBAR RESUME MEDIS</h2></th>
</tr>
  <tr style="margin:0">
  <td colspan="20" style="margin:0"><h2 style="margin-top:0;">BULAN : {{$bln}} TAHUN : {{$thn}}</h2></td>
</tr>
@endif
  <tr>
<td colspan="2">Jumlah Pasien : {{$jml_pasien}}</td>
</tr>
<table cellspacing=0 style="text-align:center;">
  <style>
    td {
      border: 1px solid #160F0F;
      text-align: center;
      min-width: 100px;
    }

    th {
      border: 1px solid #160F0F;
      text-align: center;
      min-width: 100px;
      font-style: oblique;
    }
  </style>
  <thead>
    <tr>
      <th><b>No</b></th>
      <th>ID PETUGAS</th>
      <th>No. RM</th>
      <th>NAMA</th>
      <th></th>
      <th colspan="2">TANGGAL</th>
      <th>TANGGAL</th>
      {{-- <th>STATUS</th> --}}
      <th colspan="5">IDENTITAS</th>
      <th style="border-bottom:none;">INDIKASI</th>
      <th style="border-bottom:none;">DX</th>
      <th style="border-bottom:none;">PROSEDUR</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th style="border-bottom:none;">DX</th>
      <th style="border-bottom:none;"></th>
      <th style="border-bottom:none;">Pemberian </th>
      <th style="border-bottom:none;">Pemberian </th>
      <th style="border-bottom:none;">Kondisi</th>
      <th rowspan="2">INTRUKSI</th>
      <th rowspan="2">INDIKASI</th>
      <th style="border-bottom:none;">TANGGAL</th>
      <th style="border-bottom:none;">KONDISI</th>
      <th rowspan="2">TRASPORTER</th>
      <th style="border-bottom:none;">NAMA</th>
      <th style="border-bottom:none;">TTD</th>
      <th style="border-bottom:none;">BARIS</th>
      <th style="border-bottom:none;">KOREKSI</th>
      <th style="border-bottom:none;">TGL</th>
      <th style="border-bottom:none;">KET</th>

    </tr>
    <tr>
      <th></th>
      <th></th>
      <th></th>
      <th>DOKTER</th>
      <th>RUANG</th>
      <th>MRS</th>
      <th>KRS</th>
      <th>ANALISA</th>
      {{-- <th>PENGEMBALIAN</th> --}}
      <th>NO RM</th>
      <th>NAMA</th>
      <th>TGL LAHIR</th>
      <th>JENIS KELAMIN</th>
      <th>ALAMAT</th>
      <th style="border-top:none">PX</th>
      <th style="border-top:none">MASUK</th>
      <th style="border-top:none">DIAGNOSA</th>
      <th>FISIK</th>
      <th>PENUNJANG</th>
      <th>TINDAKAN PROSEDUR</th>
      <th>ICD-9</th>
      <th style="border-top:none;">AKHIR</th>
      <th style="border-top:none;">ICD-10</th>
      <th style="border-top:none">Medikamentosa</th>
      <th style="border-top:none">obat waktu pulang</th>
      <th style="border-top:none">Pulang</th>
      <th>KONTROL</th>
      <th></th>
      <th></th>
      <th>MENDESAK</th>
      <th></th>
      <th style="border-top:none">DOKTER</th>
      <th style="border-top:none">DOKTER</th>
      <th></th>
      <th style="border-top:none"></th>
      <th style="border-top:none">PELAYANAN</th>
      <th style="border-top:none"></th>
    </tr>
    <tr>
      @for ($a=1; $a <= 8; $a++) <td style="background-color:#B4B4B4"></td>
        @endfor
      @for ($y=1; $y <= 28; $y++) <td style="background-color:#B4B4B4">{{$y}}</td>
        @endfor
        <td></td>
    </tr>
  </thead>
  <tbody>
    @foreach ($dt as $data)
    <tr>
      <td>{{$no++}}</td>
      <td>{{$data->id_petugas}}</td>
      <td style=min-width:100px>{{$data->no_rm}} </td>
      <td style=min-width:100px>{{$data->nama_dokter}}</td>
      <td style=min-width:100px>{{$data->nama_ruang}}</td>
      <td>{{$data->mrs}}</td>
      <td>{{$data->krs}}</td>
      <td>{{$data->tgl_analisa}}</td>
      {{-- <td>{{$data->status_pengembalian}}</td> --}}
      <td>{{$data->no_rm == !null ?'1':'0'}}</td>
      <td>{{$data->nama == !null ? '1' : '0'}}</td>
      <td>{{$data->tgl_lahir == !null ? '1' : '0'}}</td>
      <td>{{$data->jenis_kelamin == !null ? '1' : '0'}}</td>
      <td>{{$data->alamat == !null ? '1' : '0'}}</td>
      <td>{{$data->rek_indikasi_masuk}}</td>
      <td>{{$data->rek_diagnosa_masuk}}</td>
      <td>{{$data->rek_prosedur}}</td>
      <td>{{$data->rek_pem_fisik}}</td>
      <td>{{$data->rek_pem_penunjang}}</td>
      <td>{{$data->rek_tindakan_pros}}</td>
      <td>{{$data->rek_kode_icd9}}</td>
      <td>{{$data->rek_diagnosa_akhir}}</td>
      <td>{{$data->rek_kode_icd10}}</td>
      <td>{{$data->rek_pem_medika}}</td>
      <td>{{$data->rek_obat_pulang}}</td>
      <td>{{$data->rek_kondisi_pulang}}</td>
      <td>{{$data->rek_instruksi}}</td>
      <td>{{$data->rek_indikasi_pulang}}</td>
      <td>{{$data->rek_tgl_kontrol}}</td>
      <td>{{$data->rek_kondisi_men}}</td>
      <td>{{$data->rek_transport}}</td>
      <td>{{$data->aut_nama_dokter}}</td>
      <td>{{$data->aut_ttd_dokter}}</td>
      <td>{{$data->cat_baris}}</td>
      <td>{{$data->cat_koreksi}}</td>
      <td>{{$data->cat_tanggal}}</td>
      @if ($data->status == '1' AND $data->no_rm == !null AND
        $data->nama == !null AND $data->tgl_lahir == !null AND
        $data->jenis_kelamin ==  !null AND $data->alamat == !null)
        <td>L</td>
      @else
        <td>TL</td>

      @endif
    </tr>
    @endforeach
  </tbody>
</table>
</html>
