<?php $hal = 'data_analisis_drm' ?>
<?php $sub = 0 ?>
@extends('layouts.admin_assembling.master')
@section('title','Assembling - Detail DRM')
@section('content')


<div class="container-fluid">
  <div class="row page-titles">
    <div class="col p-0">
      <h4>Hello, {{ Auth::user()->name }} <span>Welcome back</span></h4>
    </div>
    <div class="col p-0">
    </div>
  </div>
  @if (session()->has('message'))
    <div class="alert alert-success">
      {{session()->get('message')}}
    </div>
  @endif
  <div class="col-12">
    <div class="card">
      <div class="card-body">

        <div class="table-responsive">
            {{-- <a href="{{url('print_detail_drm')}}/{{$bln}}/{{$thn}}" class="btn btn-secondary" style="float:right;"><i class="icon ion-android-download"></i></a> --}}
          <table id="example4" class="display nowrap" style="width:100%">

            <thead>
              <tr>
                <th>No</th>
                <th>No RM</th>
                <th>Nama</th>
                <th>Tgl Mrs</th>
                <th>Tgl Krs</th>
                <th>status</th>
                <th>Kelengkapan</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($data_detail as $data)
              <tr>
                <td>{{$no++}}</td>
                <td>{{$data->no_rm}}</td>
                <td>{{$data->nama}}</td>
                <td>{{date('d-m-Y',strtotime($data->mrs))}}</td>
                <td>{{date('d-m-Y',strtotime($data->krs))}}</td>
                <td>
                @if ($data->status_pengembalian == "1")
                  <span class="badge badge-primary">Belum Dikembalikan</span>
                @elseif ($data->status_pengembalian == "2")
                  <span class="badge badge-success">Tepat Waktu</span>
                @elseif ($data->status_pengembalian == "3")
                  <span class="badge badge-danger">Terlambat</span>
                @else
                  <span class="badge badge-default">Masuk Pertama</span>
                @endif
              </td>
              <td style="text-align:center;">
                @if ($data->status == 1 AND $data->no_rm == !null
                AND $data->nama == !null AND $data->tgl_lahir ==!null
                AND $data->jenis_kelamin == !null AND $data->alamat == !null)
                <span class="badge badge-success"><i class="ion ion-checkmark-round"></i></span>
                @else
                <span class="badge badge-warning"><i class="ion ion-close"></i></span>
                @endif
              </td>


                <td>
                  <form action="{{ route('data_analisis_drm.destroy',$data->no_analisa) }}" method="post">
                    {{csrf_field()}}
                    {{ method_field('DELETE') }}
                    <a style="margin:2px" href="{{route('data_analisis_drm.edit',$data->no_analisa)}}" class="btn btn-warning btn-ft" data-toggle="tooltip" data-placement="botttom" title="Edit Data"><i class="icon ion-edit"></i></a>

                    {{-- <a style="margin:2px" href="{{route('data_analisis_drm.edit',$data->month.$data->year)}}" class="btn btn-success btn-ft" data-toggle="tooltip" data-placement="botttom" title="Edit Data"><i class="icon ion-eye"></i></a> --}}

                    <button style="margin:2px" class="btn btn-danger btn-ft" onclick="return confirm('Apa Anda Yakin?');" data-toggle="tooltip" data-placement="botttom" title="Hapus Data"><i class="icon ion-trash-b"></i></button>

                  </form>

                </td>
              </tr>
              @endforeach
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
