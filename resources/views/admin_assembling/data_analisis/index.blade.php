<?php $hal = 'data_analisis_drm' ?>
<?php $sub = 0 ?>
@extends('layouts.admin_assembling.master')
@section('title','Assembling - Data Analis DRM')
@section('content')


<div class="container-fluid">
  <div class="row page-titles">
    <div class="col p-0">
      <h4>Hello, {{ Auth::user()->name }} <span>Welcome back</span></h4>

    </div>
    <div class="col p-0">
    </div>
  </div>
  @if (session()->has('message'))
    <div class="alert alert-success">
      {{session()->get('message')}}
    </div>
  @endif
  <div class="col-12">
    <div class="card">
      <div class="card-body">

        <div class="table-responsive">
          <table id="example" class="display" style="min-width: 845px">
            <thead>
              <tr>
                <th>No</th>
                <th>No. RM</th>
                <th>Nama Pasien</th>
                <th>Tgl. Masuk</th>
                <th>Tgl. Keluar</th>
                <th style="text-align:center;">Lengkap</th>
                <th style="text-align:center;">Aksi</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($data_a as $data)
              <tr>
                <td>{{$no++}}</td>
                <td>{{$data->no_rm}}</td>
                <td>{{$data->nama}}</td>
                <td>{{date('d-m-Y',strtotime($data->mrs))}}</td>
                <td>{{date('d-m-Y',strtotime($data->krs))}}</td>

                <td style="text-align:center;">
                  @if ($data->status == 1 AND $data->no_rm == !null
                  AND $data->nama == !null AND $data->tgl_lahir ==!null
                  AND $data->jenis_kelamin == !null AND $data->alamat == !null)
                  <span class="badge badge-success"><i class="ion ion-checkmark-round"></i></span>
                  @else
                  <span class="badge badge-warning"><i class="ion ion-close"></i></span>
                  @endif
                </td>
                <td>
                  <form method="post" action="{{route('data_analisis_drm.destroy',$data->no_analisa)}}">
                    {{csrf_field()}}
                    {{ method_field('DELETE') }}
                    <a style="margin:2px" href="{{route('data_analisis_drm.edit',$data->no_analisa)}}" class="btn btn-warning btn-ft" data-toggle="tooltip" data-placement="botttom" title="Edit Data"><i class="icon ion-edit"></i></a>
                    {{-- <a style="margin:2px" href="{{route('data_analisis_drm.show',$data->no_rm)}}" class="btn btn-success btn-ft" data-toggle="tooltip" data-placement="botttom" title="Detail"><i class="icon ion-eye"></i></a> --}}
                    <button style="margin:2px" class="btn btn-danger btn-ft" onclick="return confirm('Apa Anda Yakin?');" data-toggle="tooltip" data-placement="botttom" title="Hapus Data"><i class="icon ion-trash-b"></i></button>

                  </form>

                </td>
              </tr>
              @endforeach
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
