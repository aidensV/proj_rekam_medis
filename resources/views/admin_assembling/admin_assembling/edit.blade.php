<?php $hal = 'admin_assembling' ?>
<?php $sub = 0 ?>
@extends('layouts.admin_assembling.master')
@section('content')




<div class="container-fluid">
  <div class="row page-titles">
    <div class="col p-0">
      <h4>Hello, {{ Auth::user()->nama_petugas }} <span>Welcome here</span></h4>
    </div>
    <div class="col p-0">

    </div>
  </div>
  <div class="col-xl-10">
    <div class="card forms-card">
      <div class="card-body">
        <h4 class="card-title mb-4">Ubah Pengguna Admin Assembling</h4>
        <div class="basic-form">
          <form action="{{route('admin_assembling.update',$data->id_petugas)}}" method="post">
            {{csrf_field()}}
            {{ method_field('PUT') }}


            <div class="form-group">
              <label class="text-label">Nama*</label>
              <input type="text" name="admin_assembling_nama" value="{{$data->nama_petugas}}" class="form-control" placeholder="Dr.Jhony" required>
            </div>
            <div class="form-group">
              <label class="text-label">Username*</label>
              <input type="text" name="admin_assembling_username" value="{{$data->username}}" class="form-control" required>
            </div>
            <div class="form-group">
              <label class="text-label">Password Baru*</label>
              <input type="password" name="admin_assembling_password" class="form-control">
            </div>


            <button type="submit" class="btn btn-primary btn-form mr-2">Simpan</button>
            <a href="{{url('admin_assembling')}}" class="btn btn-light text-dark btn-form">Batal</a>
            {{-- <button type="button" class="btn btn-light text-dark btn-form">Cancel</button> --}}
          </form>
        </div>
      </div>
    </div>
  </div>


</div>
@endsection
