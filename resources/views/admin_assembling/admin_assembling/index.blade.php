<?php $hal = 'admin_assembling' ?>
<?php $sub = 0 ?>
@extends('layouts.admin_assembling.master')
@section('content')


<div class="container-fluid">
  <div class="row page-titles">
    <div class="col p-0">
      <h4>Hello, {{ Auth::user()->nama_petugas }} <span>Welcome back</span></h4>
    </div>
    <div class="col p-0">
    </div>
  </div>
  <a href="{{route('admin_assembling.create')}}" style="margin-left:15px;margin-bottom:10px;" class="btn btn-primary btn-ft">Tambah Data</a>


  <div class="col-12">
    <div class="card">
      <div class="card-body">
        <div class="table-responsive">
          <table id="example" class="display" style="min-width: 845px">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Username</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($admin_assembling as $data)
              <tr>
                <td>{{$no++}}</td>
                <td>{{$data->nama_petugas}}</td>
                <td>{{$data->username}}</td>
                <td>
                  <form action="{{ route('admin_assembling.destroy', $data->id_petugas) }}" method="post">
                    {{csrf_field()}}
                    {{ method_field('DELETE') }}
                    {{-- <a href="{{route('owner_marketing.show',$data->id_produk)}}" style="margin-right:10px;" class="btn btn-primary" data-toggle="tooltip" data-placement="botttom" title="Lihat Detail"><i class="fa fa-eye"></i></a> --}}
                    <a href="{{route('admin_assembling.edit',$data->id_petugas)}}" class="btn btn-warning btn-ft" data-toggle="tooltip" data-placement="botttom" title="Edit Data"><i class="icon ion-edit"></i></a>
                    <button class="btn btn-danger btn-ft" onclick="return confirm('Apakah Anda yakin data akan dihapus?');" data-toggle="tooltip" data-placement="botttom" title="Hapus Data"><i class="icon ion-trash-b"></i></button>

                  </form>

                </td>
              </tr>
              @endforeach
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
