<?php $hal = 'chart_ruangan' ?>
<?php $sub = 0 ?>
@extends('layouts.admin_assembling.master')
@section('title','Assembling - Data Chart')
@section('content')

<div class="container-fluid">
  <div class="row page-titles">
    <div class="col p-0">
      <h4>Hello, {{ Auth::user()->name }} <span>Welcome back</span></h4>
    </div>
    <div class="col p-0">
    </div>
  </div>
  <div class="card col-lg-12">
    <div class="card-body">
      <form class="form-inline" id="form_cari" action="{{url('data_analisis_chart_ruang/cari')}}" method="get">

        <div class="col-lg-3 mb-4" >
          <div class="form-group">
            <label class="text-label">Nama Ruangan*</label>
            <select style="width: 100%" onchange="select_ruangan(this.value)" class="form-control" name="ruangan_nama" id="ruangan_nama">
              <option value="all">Semua</option>
              @foreach ($dta_ruangan as $data)
            <option value="{{$data->id_ruang}}">{{$data->nama_ruang}}</option>
              @endforeach

            </select>
          </div>
        </div>
        <div class="col-lg-3 mb-4" >
            <div class="form-group">
              <label class="text-label">Dari*</label>
              <input style="width: 90%" type="text" name="cari_awal" id="cari_awal" autocomplete="off" value="{{ old('cari_awal') }}"  class="form-control mydatepicker" data-date-format="dd/mm/yyyy" placeholder="tanggal/bulan/tahun" required>
            </div>
          </div>
        <span style="margin-left: opx; margin-right: 2%; margin-top: 17px; "> <h2>-</h2></span>
        <div class="col-lg-3 mb-4" >
          <div class="form-group">
            <label class="text-label">Sampai*</label>
            <input style="width: 90%" type="text" name="cari_akhir" id="cari_akhir" autocomplete="off" value="{{ old('cari_akhir') }}" class="form-control mydatepicker" data-date-format="dd/mm/yyyy" placeholder="tanggal/bulan/tahun" required>
          </div>
        </div>
        <div class="col-lg-2 mb-4">
          <div class="form-group">
            {{-- <a href="{{url('data_analisis_chart_ruang/cari')}}" class="btn btn-dark" value="cari" name="button"><i class="icon ion-ios-search-strong"></i></a> --}}
            <button type="submit"  style="margin-top:20%" id="btn_cari" class="btn btn-dark" value="cari" name="button"><i class="icon ion-ios-search-strong"></i></button>
          </div>
        </div>
        @if ($jmlh_ruang > 0)
        <div class="col-lg-2 mb-4">
          <div class="form-group">
            <a href="{{url('print_ruang')}}/{{$awal}}/{{$akhir}}/{{$id_ruang}}" class="btn btn-secondary" style="float:right;"><i class="icon ion-android-download"></i></a>
          </div>
        </div>
      @endif
      </form>
    </div>
  </div>
  {{-- <a href="{{route('data_pasien.create')}}" style="margin-left:15px;margin-bottom:10px;" class="btn btn-primary btn-ft">Tambah Data</a> --}}


  <div class="col-12">
    <div class="card">
      <div class="card-body">
        <div style="margin-bottom:3%;">
        @if ($awal && $akhir && $nama_ruang == !null)
          <h4 style="text-align:center" class="card-title">LAPORAN ANALISA KUANTITATIF LEMBAR RESUME MEDIS PER RUANG : {{$nama_ruang}} </h4>
          <h4 style="text-align:center">periode {{date('d-M-Y',strtotime($awal))}} s/d {{date('d-M-Y',strtotime($akhir))}}</h4>
        @elseif ($awal && $akhir  == !null)
          <h4 style="text-align:center" class="card-title">LAPORAN ANALISA KUANTITATIF LEMBAR RESUME MEDIS PER RUANG  </h4>
          <h4 style="text-align:center" > periode {{date('d-M-Y',strtotime($awal))}} s/d {{date('d-M-Y',strtotime($akhir))}}</h4>

        @else
          <h4 style="text-align:center" class="card-title">LAPORAN ANALISA KUANTITATIF LEMBAR RESUME MEDIS PER RUANG  </h4>
          <h4 style="text-align:center" > Bulan: {{$bln}} Tahun: {{$thn}}</h4>

        @endif
        <button type="button" id="download-pdf2" class="btn btn-secondary"><i class="icon ion-android-download"></i></button>
      </div>
        {{-- <button type="button" id="download-pdf2" class="btn btn-secondary"><i class="icon ion-android-download"></i></button> --}}
        <canvas id="barChart"></canvas>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script src="{{asset('public/assets/plugins/chart.js/Chart.bundle.min.js')}}"></script>
<script>
  function select_ruangan(val){
    var ruangan = val;
    if (ruangan == "all") {
      document.getElementById("form_cari").action="{{url('data_analisis_chart_ruang/cari')}}";
    }else{
      document.getElementById("form_cari").action="{{url('data_analisis_chart_ruang/cari/ruangan')}}";
    }

    console.log(ruangan)
  };
  var ctx = document.getElementById("barChart");
  var lengkap = <?php echo $total_lengkap; ?>;
  var tidak_lengkap = <?php echo $total_tidak_lengkap; ?>;
  var ruangan = <?php echo $ruangan; ?>;

  ctx.height = 100;
  new Chart(ctx, {
    type: 'bar',

    data: {
      labels:ruangan ,
      datasets: [{
          label: "Data Lengkap",
          data: lengkap,
          borderColor: "#34C73B",
          borderWidth: "0",
          backgroundColor: "rgba(52, 199, 59, .4)"
        },
        {

          label: "Data Tidak Lengkap",
          data: tidak_lengkap,
          borderColor: "#00A2FF",
          borderWidth: "0",
          backgroundColor: "rgba(0, 162, 255, .4)"
        }
      ]
    },
    options: {
      tooltips: {

               callbacks: {
                   label: function (tooltipItem, details) {

                           let dataset = details.datasets[tooltipItem.datasetIndex];
                           let currentValue = dataset.data[tooltipItem.index];
                           return dataset.label + ": " + currentValue + " %";
                           // console.log(dataset.data[0]);
                   },
                   afterLabel: function(tooltipItem, data) {
                     let dataset = data.datasets[tooltipItem.datasetIndex];
                     let currentValue = dataset.data[tooltipItem.index];
                     let p = <?php echo $jmlh_pasien ?>;
                     let jumlah_pasien =((dataset['data'][tooltipItem['index']] / 100) * p);
                       let x = <?php echo $jmlh_ruang ?>;
                       if (x == 0) {
                         let y = <?php echo $jmlh ?>;
                         if (tooltipItem.datasetIndex == 1) {
                           return 'Jumlah DRM: ' + Math.round((currentValue/100) * y[tooltipItem.index]);

                         }else {
                           return 'Jumlah DRM: ' + Math.round((currentValue/100) * y[tooltipItem.index]);
                         }
                       }
                       let jumlah =((dataset['data'][tooltipItem['index']] / 100) * x);
                       // console.log(tooltipItem.index);
                       if (tooltipItem.index == 0) {
                         if (tooltipItem.datasetIndex) {
                           return 'Jumlah Identitas Tidak Lengkap: ' + Math.round(jumlah_pasien);
                         }
                         else{

                           return 'Jumlah Identitas Lengkap: ' + Math.round(jumlah_pasien);
                         }

                       }else{

                         if (tooltipItem.datasetIndex) {
                           return 'Jumlah Tidak Lengkap: ' + Math.round(jumlah);

                         }else{

                           return 'Jumlah Lengkap: ' + Math.round(jumlah);
                         }

                     }

                      }
               }
           },
      legend: {
           display: true,
           position:'bottom',
           labels: {
               fontFamily:"'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
               fontColor: 'rgb(000, 000, 000)'
           }
       },
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }],
        xAxes: [{
          // Change here
          barPercentage: 0.2
        }]
      }
    }
  });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.debug.js"></script>
<script>

document.getElementById('download-pdf2').addEventListener("click", downloadPDF2);

  function downloadPDF2() {
    var newCanvas = document.querySelector('#barChart');

    //create image from dummy canvas
    var newCanvasImg = newCanvas.toDataURL("image/png", 1.0);

    //creates PDF from img
    var doc = new jsPDF('landscape');
    doc.setFontSize(20);
    doc.text(15, 10, "Data Kelengkapan Ruangan");
    doc.addImage(newCanvasImg, 'PNG', 10, 15, 280, 150);
    doc.save('chart.pdf');
  }

  // function PrintImage() {
  //   var canvas = document.getElementById("barChart");
  //   var win = window.open();
  //   win.document.write("<br><img src='" + canvas.toDataURL() + "'/>");
  //   win.print();
  //   win.location.reload();
// }

</script>
@endsection
