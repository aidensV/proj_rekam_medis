<?php $hal = 'data_analisis_drm' ?>
<?php $sub = 0 ?>
@extends('layouts.admin_assembling.master')
@section('title','Assembling - Data Chart')
@section('content')


<div class="container-fluid">
  <div class="row page-titles">
    <div class="col p-0">
      <h4>Hello, {{ Auth::user()->name }} <span>Welcome back</span></h4>
    </div>
    <div class="col p-0">
    </div>
  </div>
  <div class="card col-lg-12">
    <div class="card-body">
      <form class="form-inline"  action="{{url('data_analisis_drm_chart/cari')}}" method="get">

        <div class="col-xs-4 col-lg-4 mb-4" >
          <div class="form-group">
            <label class="text-label">Dari</label>
            <input type="text" name="cari_awal" autocomplete="off" value="{{ old('cari_awal') }}"  class="form-control mydatepicker" data-date-format="dd/mm/yyyy" placeholder="tanggal/bulan/tahun" required>

          </div>
        </div>
        <span style="margin-left: 2%; margin-top: 2%; position: relative"> <h2>-</h2></span>
        <div class="col-xs-4 col-lg-4 mb-4" >
          <div class="form-group">
            <label class="text-label">Sampai</label>
            <input type="text" name="cari_akhir" autocomplete="off" value="{{ old('cari_akhir') }}" class="form-control mydatepicker" data-date-format="dd/mm/yyyy" placeholder="tanggal/bulan/tahun" required>
          </div>
        </div>
        <div class="col-lg-3 mb-4">
          <div class="form-group" style="margin-top:10%;margin-left:5px;">
            <button type="submit" class="btn btn-dark" value="cari"><i class="icon ion-ios-search-strong"></i></button>
          </div>
        </div>
        <div class="col-lg-10 mb-4" >
          <div class="form-group">
            @if ($awal && $akhir == !null)
              <a href="{{url('print_detail_drm/cari')}}/{{$awal}}/{{$akhir}}" class="btn btn-secondary" style="float:right;"><i class="icon ion-android-download"></i></a>
          @else
            <a href="{{url('print_detail_drm')}}/{{$bln}}/{{$thn}}" class="btn btn-secondary" style="float:right;"><i class="icon ion-android-download"></i></a>

          @endif
          </div>

        </div>
      </form>
    </div>
  </div>
  {{-- <a href="{{route('data_pasien.create')}}" style="margin-left:15px;margin-bottom:10px;" class="btn btn-primary btn-ft">Tambah Data</a> --}}

    <div class="card class="col-12"">
      <div class="card-body">
        <div style="margin-bottom:3%">
          {{-- {{$awal}} --}}
      @if ($awal && $akhir == !null)
        <h4 style="text-align:center" class="card-title">LAPORAN ANALISA KUANTITATIF LEMBAR RESUME MEDIS </h4>
        <h4 style="text-align:center">periode {{date('d-m-Y',strtotime($awal))}} s/d {{date('d-m-Y',strtotime($akhir))}}</h4>
      @else
        <h4 style="text-align:center" class="card-title">LAPORAN ANALISA KUANTITATIF LEMBAR RESUME MEDIS</h4>
        <h4 style="text-align:center"> Tahun: {{$thn}}</h4>
      @endif
        <button type="button" id="download-pdf2" class="btn btn-secondary"><i class="icon ion-android-download"></i></button>
      </div>
        <canvas id="barChart" ></canvas>
      </div>
    </div>

</div>
@endsection
@section('js')
<script src="{{asset('public/assets/plugins/chart.js/Chart.bundle.min.js')}}"></script>
<script src="{{asset('public/assets/plugins/select2/js/select2.full.min.js')}}"></script>
<script src="{{asset('public/assets/js/plugins-init/select2-init.js')}}"></script>
<script>
  var ctx = document.getElementById("barChart");
  var lengkap = <?php echo $lengkap_avg; ?>;
  var tidak_lengkap = <?php  echo $tidak_lengkap_avg; ?>;
  var nama_bulan = <?php echo $nama_bulan ?>;
  if (nama_bulan.length >= 0) {
    var label = nama_bulan;
  }else {
    var label = ["Identitas","Ind Px", "dx Msk", "Prosedur", "Fisik", "Penunjang", "Tind Pros", "Icd 9", "Dx Akhir", "Icd 10", "Medika",
    "Obat Plg", "knd plg", "Inst",
     "Indikasi", "Tgl Ktrl", "Knd Mndsk", "Trans","Nama Dok","Ttd Dok","Baris","Korksi","tgl" ];
  }
  ctx.height = 100;
  new Chart(ctx, {
    type: 'bar',

    data: {
      labels: label,
      datasets: [{
          label: "Data DRM Lengkap",
          data: lengkap,
          borderColor: "#34C73B",
          borderWidth: "0",
          backgroundColor: "rgba(52, 199, 59, .4)"
        },
        {

          label: "Data DRM Tidak Lengkap",
          data: tidak_lengkap,
          borderColor: "#00A2FF",
          borderWidth: "0",
          backgroundColor: "rgba(0, 162, 255, .4)"
        }
      ]
    },
    options: {
      tooltips: {

               callbacks: {
                   label: function (tooltipItem, details) {
                       if (tooltipItem.datasetIndex == 1) {
                           let dataset = details.datasets[tooltipItem.datasetIndex];
                           let currentValue = dataset.data[tooltipItem.index];
                           // console.log(currentValue);
                           // console.log(dataset);
                           return dataset.label + ": " + currentValue + " %";

                       } else {
                           let dataset = details.datasets[tooltipItem.datasetIndex];
                           let currentValue = dataset.data[tooltipItem.index];
                           return dataset.label + ": " + currentValue +" %";
                       }
                   },
                   afterLabel: function(tooltipItem, data) {
                        // let dataset = data['datasets']['index'];
                        var dataset = data.datasets[tooltipItem.datasetIndex];
                        var currentValue = dataset.data[tooltipItem.index];
                        // console.log((dataset.data['index'])
                        var x = <?php echo $jumlah ?>;
                        // console.log(currentValue);
                        // console.log(x);
                        if (nama_bulan.length > 0) {
                          if (tooltipItem.datasetIndex == 1) {
                            return 'Jumlah DRM: ' + Math.round((currentValue / 100) * x[tooltipItem.index]);

                          }else {
                            return 'Jumlah DRM: ' + Math.round((currentValue / 100) * x[tooltipItem.index]);
                          }

                        }else {


                          let y = <?php echo $jumlah_pasien ?>;
                          let jumlah_pasien =((dataset['data'][tooltipItem['index']] / 100) * y);
                          // console.log(tooltipItem.datasetIndex)


                          let jumlah =((dataset['data'][tooltipItem['index']] / 100) * x);
                          // console.log(tooltipItem.index);
                          if (tooltipItem.index == 0) {
                            if (tooltipItem.datasetIndex) {
                              return 'Jumlah Identitas Tidak Lengkap: ' + Math.round(jumlah_pasien);
                            }
                            else{

                              return 'Jumlah Identitas Lengkap: ' + Math.round(jumlah_pasien);
                            }

                          }else{

                            if (tooltipItem.datasetIndex) {
                              return 'Jumlah Tidak Lengkap: ' + Math.round(jumlah);

                            }else{

                              return 'Jumlah Lengkap: ' + Math.round(jumlah);
                            }

                        }
                      }

                  }


               }
           },
      legend: {
           display: true,
           position:'bottom',
           labels: {
               fontFamily:"'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
               fontColor: 'rgb(000, 000, 000)'
           }
       },
       layout: {
         padding:{
           left:0,
           right:0,
           top:0,
           bottom:0
         }
       },
      scales: {
        yAxes: [{
          barPercentage: 0.2,
          categoryPercentage: 0.2,
          ticks: {
            beginAtZero: true

          }
        }],
        xAxes: [{

          barPercentage: 0.2,
        }]
      }
    }
  });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.debug.js"></script>
<script>

document.getElementById('download-pdf2').addEventListener("click", downloadPDF2);

  function downloadPDF2() {
    var newCanvas = document.querySelector('#barChart');

    //create image from dummy canvas
    var newCanvasImg = newCanvas.toDataURL("image/png", 1.0);
    //creates PDF from img
    var doc = new jsPDF('landscape');
    doc.setFontSize(20);
    doc.text(15, 10, "Diagram Kelengkapan Rekam Medis");
    doc.addImage(newCanvasImg, 'PNG', 10, 15, 280, 150);
    doc.save('chart.pdf');
  }

  // function PrintImage() {
  //   var canvas = document.getElementById("barChart");
  //   var win = window.open();
  //   win.document.write("<br><img src='" + canvas.toDataURL() + "'/>");
  //   win.print();
  //   win.location.reload();
// }

</script>
@endsection
