<?php $hal = 'data_pasien' ?>
<?php $sub = 0 ?>
@extends('layouts.admin_assembling.master')
@section('title','Assembling - Data Pasien')
@section('content')


<div class="container-fluid">
  <div class="row page-titles">
    <div class="col p-0">
      <h4>Hello, {{ Auth::user()->nama_petugas }} <span>Welcome back</span></h4>
    </div>
    <div class="col p-0">
    </div>
  </div>
  <a href="{{route('data_pasien.create')}}" style=" font-family:Times New Roman ;margin-left:15px;margin-bottom:10px;" class="btn btn-primary btn-ft">Tambah Data</a>


  <div class="col-12">
    <div class="card">
      <div class="card-body">
        <div class="table-responsive">
          <table id="example" class="display" style="min-width: 845px">
            <thead>
              <tr>
                <th>No</th>
                <th>No RM</th>
                <th>Nama</th>
                <th>Jenis Kelamin</th>
                <th>Tanggal Lahir</th>
                <th style="text-align:center;">Aksi</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($data_pasien as $data)
              <tr>
                <td>{{$no++}}</td>
                <td>{{$data->no_rm}}</td>
                <td>{{$data->nama}}</td>
                @if($data->jenis_kelamin=='P')
                  <td>{{$dtjk='Perempuan'}}</td>
                @elseif($data->jenis_kelamin=='L')
                  <td>{{$dtjk='Laki-Laki'}}</td>
                @else
                  <td><span class="badge badge-warning">-</span></td>
                @endif
                @if ($data->tgl_lahir != null)
                  <td>{{date('d-m-Y',strtotime($data->tgl_lahir))}}</td>
                @else
                  <td><span class="badge badge-warning">-</span></td>
              @endif
                <td>
                  <form action="{{ route('data_pasien.destroy', $data->no_rm) }}" method="post">
                    {{csrf_field()}}
                    {{ method_field('DELETE') }}
                    {{-- <a href="{{route('owner_marketing.show',$data->id_produk)}}" style="margin-right:10px;" class="btn btn-primary" data-toggle="tooltip" data-placement="botttom" title="Lihat Detail"><i class="fa fa-eye"></i></a> --}}
                    <a style="margin:2px" href="{{route('data_pasien.edit',$data->no_rm)}}" class="btn btn-warning btn-ft" data-toggle="tooltip" data-placement="botttom" title="Edit Data"><i class="icon ion-edit"></i></a>

                    <a style="margin:2px" href="{{route('data_pasien.show',$data->no_rm)}}" class="btn btn-success btn-ft" data-toggle="tooltip" data-placement="botttom" title="Edit Data"><i class="icon ion-eye"></i></a>

                    <button style="margin:2px" class="btn btn-danger btn-ft" onclick="return confirm('Apakah Anda yakin data akan dihapus?');" data-toggle="tooltip" data-placement="botttom" title="Hapus Data"><i class="icon ion-trash-b"></i></button>

                  </form>

                </td>
              </tr>
              @endforeach
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
