<?php $hal = 'data_pasien' ?>
<?php $sub = 0 ?>
@extends('layouts.admin_assembling.master')
@section('title','Assembling - Edit Data Pasien')
@section('content')




<div class="container-fluid">
  <div class="row page-titles">
    <div class="col p-0">
      <h4>Hello, {{ Auth::user()->nama_petugas }} <span>Welcome here</span></h4>
    </div>
    <div class="col p-0">

    </div>
  </div>
  <div class="col-xl-10">
    <div class="card forms-card">
      <div class="card-body">
        <h4 class="card-title mb-4">Ubah Data Pasien</h4>
        <div class="basic-form">
          <form action="{{route('data_pasien.update',$data->no_rm)}}" method="post">
            {{ csrf_field() }}
            {{ method_field('PUT') }}

            <div class="form-group">
              <label class="text-label">No RM*</label>
              <input type="text" name="pasien_no_rm" value="{{$data->no_rm}}" class="form-control" required>
            </div>
            <div class="form-group">
              <label class="text-label">Nama Pasien*</label>
              <input type="text" name="pasien_nama" value="{{$data->nama}}" class="form-control" placeholder="Mrs.Shinta" required>
            </div>
            <div class="form-group">
              <label class="text-label">Jenis kelamin*</label>
              <div class="col-sm-6 col-md-4 col-xl">
                <div class="form-radio">

                  <input id="radio15" class="" name="pasien_jk" <?php if($data->jenis_kelamin == "L") echo "checked" ?> value="L" type="radio">
                  <label for="radio15" class="radio-green">Laki-Laki</label>

                  <input id="radio14" class="" name="pasien_jk" <?php if($data->jenis_kelamin == "P") echo "checked" ?> value="P" type="radio">
                  <label for="radio14" class="radio-green">Perempuan</label>

                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="text-label">Tanggal Lahir*</label>
              <input type="text" name="pasien_tgl_lahir" autocomplete="off" value="{{date('d/m/Y',strtotime($data->tgl_lahir)) }}" class="form-control mydatepicker" data-date-format="dd/mm/yyyy" placeholder="tanggal/bulan/tahun" required>
            </div>
            <div class="form-group">
              <label class="text-label">Alamat*</label>
              <textarea name="pasien_alamat" class="form-control" rows="8" cols="80">{{$data->alamat}}</textarea>
            </div>

            <button type="submit" class="btn btn-primary btn-form mr-2">Simpan</button>
            <a href="{{url('data_pasien')}}" class="btn btn-light text-dark btn-form">Batal</a>
            {{-- <button type="button" class="btn btn-light text-dark btn-form">Cancel</button> --}}
          </form>
        </div>
      </div>
    </div>
  </div>


</div>
@endsection
