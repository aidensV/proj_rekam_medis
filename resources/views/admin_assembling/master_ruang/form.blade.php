<div class="modal fade" id="modal-form">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"></h5>
        <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
        </button>
      </div>
      <form class="form-horizontal" data-toggle="validator" method="post">
        {{ csrf_field() }} {{ method_field('POST') }}
        <div class="modal-body">
          <input type="hidden" id="id" name="id">
          {{-- <div class="form-group ">
            <label class="text-label">Nama Kamar*</label>
            <select  class="form-control" name="kamar_nama" required>
              <option class="text-muted "  disabled="true" selected="true" style="display: none">Pilih Kamar</option>
              @foreach ($data_kamar as $value)
              <option value="{{$value->kamar_id}}">{{$value->kamar_nama}}</option>
              @endforeach
            </select>
          </div> --}}
          <div class="form-group">
            <label class="text-label">Nama Ruang*</label>
            <input id="ruangan_nama" type="text" name="ruangan_nama" class="form-control" required>
          </div>
          <div class="form-group">
            <label class="text-label">Kelas*</label>
            <input id="ruangan_kelas" type="text" name="ruangan_kelas" class="form-control" required>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>
