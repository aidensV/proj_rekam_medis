<?php $hal = 'data_ruang' ?>
<?php $sub = 0 ?>
@extends('layouts.admin_assembling.master')
@section('content')


<div class="container-fluid">
  <div class="row page-titles">
    <div class="col p-0">
      <h4>Hello, {{ Auth::user()->nama_petugas }} <span>Welcome back</span></h4>
    </div>
    <div class="col p-0">
    </div>
  </div>
  <a onclick="addForm()"> <button style="margin-left:15px;margin-bottom:10px;" class="btn btn-primary btn-ft">Tambah Data</button></a>


  <div class="col-12">
    <div class="card">
      <div class="card-body">
        <div class="table-responsive">
          <table id="datatable1"  class="table display responsive nowrap">
              <thead>
                <tr>
                  <th class="wd-15p">No</th>
                  <th class="wd-15p">Kelas</th>
                  {{-- <th class="wd-15p">Nama Kamar</th> --}}
                  <th class="wd-15p">Nama Ruang</th>
                  <th class="wd-15p">Action</th>
                </tr>
              </thead>
              <tbody>

              </tbody>
            </table>
        </div><!-- table-wrapper -->
        @include('admin_assembling.master_ruang.form')

      </div>
    </div>
  </div>
</div>
@endsection


@section('js')
  <!-- <script src="{{ asset('admin/lib/jquery-ui/jquery-ui.js') }}"></script> -->

  <script src="{{ asset('public/assets/js/validator.js') }}"></script>
  <script src="{{ asset('public/assets/plugins/highlightjs/highlight.pack.js') }}"></script>
@endsection

@section('script')

<script type="text/javascript">
	var table, save_method;
	$(function(){
		table = $('.table').DataTable({
			"processing" : true,
			"ajax" : {
				"url" : "{{ route('master_ruang') }}",
				"type" : "GET"
			}
		});

		$('#modal-form form').validator().on('submit', function(e){
      if(!e.isDefaultPrevented()){
         var id = $('#id').val();
         if(save_method == "add") url = "{{ route('data_ruang.store') }}";
         else url = "data_ruang/"+id;

         $.ajax({
           url : url,
           type : "POST",
           data : $('#modal-form form').serialize(),
           success : function(data){
             $('#modal-form').modal('hide');
             table.ajax.reload();
           },
           error : function(){
             alert("Tidak dapat menyimpan data!");
           }
         });
         return false;
     }
   });


	});

	function addForm(){
		save_method = "add";
		$('input[name=_method]').val('POST');
		$('#modal-form').modal('show');
		$('#modal-form form')[0].reset();
		$('.modal-title').text('Tambah Ruang');
	}

	function editForm(id){
		save_method = "edit";
		$('input[name=_method]').val('PATCH');
		$('#modal-form form')[0].reset();
		$.ajax({
			url : "data_ruang/"+id+"/edit",
			type : "GET",
			dataType : "JSON",
			success : function(data){
				$('#modal-form').modal('show');
				$('.modal-title').text('Edit Ruang');

				$('#id').val(data.id_ruang);

				$('#ruangan_nama').val(data.nama_ruang);
				$('#ruangan_kelas').val(data.kelas_ruang);
			},
			error : function(){
				alert("Tidak dapat menampilkan data !!!");
			}
		});
	}


  function deleteData(id){
   if(confirm("Apakah Anda yakin data akan dihapus?")){
     $.ajax({
       url : "data_ruang/"+id,
       type : "POST",
       data : {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
       success : function(data){
         table.ajax.reload();
       },
       error : function(){
         alert("Tidak dapat menghapus data!");
       }
     });
   }
}

</script>

@endsection
