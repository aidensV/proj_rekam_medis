<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>@yield('title')</title>
  <!-- Favicon icon -->
  <link rel="icon" type="image/png" sizes="16x16" href="{{asset('public/assets/images/favicon.png')}}">
  <!-- Custom Stylesheet -->
  <link href="{{asset('public/assets/plugins/jquery-steps/css/jquery.steps.css')}}" rel="stylesheet">
  <link rel="stylesheet" href="{{asset('public/assets/plugins/select2/css/select2.min.css')}}">

  <link href="{{asset('public/assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
  <link href="{{asset('public/assets/plugins/datatables/css/jquery.dataTables.min.css')}}" rel="stylesheet">
  <link href="{{asset('public/assets/css/style.css')}}" rel="stylesheet">

  {{-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" /> --}}

</head>

<body>

  <!--*******************
      Preloader start
  ********************-->
  <div id="preloader">
    <div class="loader">
      <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
      </svg>
    </div>
  </div>
  <!--*******************
      Preloader end
  ********************-->


  <!--**********************************
      Main wrapper start
  ***********************************-->
  <div id="main-wrapper">

    <!--**********************************
            Nav header start
        ***********************************-->
    <div class="nav-header">
      {{-- <div class="brand-logo"><a href="index.html"><b><img src="{{asset('public/assets/images/logo.png')}}" alt="">--}}
      {{-- </b><span class="brand-title"><img src="{{asset('public/assets/images/logo-text.png')}}" alt=""></span></a>--}}
      {{-- </div>--}}
      <div class="nav-control">
        <div class="hamburger"><span class="line"></span> <span class="line"></span> <span class="line"></span>
        </div>
      </div>
    </div>
    <!--**********************************
            Nav header end
        ***********************************-->

    <!--**********************************
            Header start
        ***********************************-->
    <div class="header">
      <div class="header-content">
        <div class="header-left">
          <ul>
            <li class="icons position-relative"><a href="javascript:void(0)"><i class="icon-magnifier f-s-16"></i></a>
              <div class="drop-down animated bounceInDown">
                <div class="dropdown-content-body">
                  <div class="header-search" id="header-search">
                    <form action="#">
                      <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search">
                        <div class="input-group-append"><span class="input-group-text"><i class="icon-magnifier"></i></span>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </li>
          </ul>
        </div>
        <div class="header-right">
          <ul>


            <li class="icons">
              <a href="javascript:void(0)" class="log-user">
                <img src="{{asset('public/assets/images/avatar/1.jpg')}}" alt="">
                <span>{{ Auth::user()->email }}</span> <i class="fa fa-caret-down f-s-14" aria-hidden="true"></i>
              </a>
              <div class="drop-down dropdown-profile animated bounceInDown">
                <div class="dropdown-content-body">
                  <ul>
                    <li><a href="{{url('change_user')}}/{{ Auth::user()->id_petugas}}"><i class="icon-user"></i> <span>My Profile</span></a>
                    {{-- <li> <button type="button" data-toggle="modal" data-target="#modal-form" name="button"> <i class="icon-user"></i> <span>My Profile</span></button> --}}
                    </li>

                    <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();"><i class="icon-power"></i>
                        <span>Logout</span></a>
                    </li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                    </form>
                  </ul>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <!--**********************************
            Header end
        ***********************************-->

    <!--**********************************
            Sidebar start
        ***********************************-->
    <div class="nk-sidebar">
      <div class="nk-nav-scroll">
        <ul class="metismenu" id="menu">

          <li class="mega-menu mega-menu-lg">

            <a href="{{route('dash_all', [Auth::user()->level])}}" aria-expanded="false" class="<?php if ($hal == " dashboard") echo "active"; ?>">
              <i class="mdi mdi-view-dashboard"></i><span class="nav-text">Home</span>
            </a>

          </li>
          <li class="nav-label">Data Master</li>
          @if (Auth::user()->level == '1')
          <li><a href="{{url('admin_assembling')}}" aria-expanded="false" class="<?php if($hal == " admin_assembling") echo "active" ; ?>">
            <i class="mdi mdi-account"></i> <span class="nav-text">Data Pengguna</span></a>
          </li>
          <li><a href="{{url('data_dokter')}}" aria-expanded="false" class="<?php if($hal == " data_dokter") echo "active" ; ?>">
              <i class="mdi mdi-account-star"></i> <span class="nav-text">Data Dokter</span></a>
          </li>

          <li><a href="{{url('data_ruang')}}" aria-expanded="false" class="<?php if($hal == " data_ruangan") echo "active" ; ?>">
              <i class="mdi mdi-hospital-building"></i> <span class="nav-text">Data Ruangan</span></a>
          </li>

        @endif

          <li class="mega-menu mega-menu-lg">
            <a href="{{url('data_pasien')}}" aria-expanded="false" class="<?php if ($hal == " data_pasien") echo "active"; ?>">
              <i class="mdi mdi-account-card-details"></i><span class="nav-text">Data Pasien</span>
            </a>
          </li>


          <li class="nav-label">Data Analisa</li>

          <li class="mega-menu mega-menu-lg ">
            <a href="{{url('analisis_drm')}}" aria-expanded="false" class="<?php if ($hal == " analisis_drm") echo "active"; ?>">
              <i class="mdi  mdi-pulse"></i><span class="nav-text">Analisa Kuantitatif RM</span>
            </a>
          </li>



          <li class="mega-menu mega-menu-lg">
            <a href="{{url('data_analisis_drm')}}" aria-expanded="false" class="<?php if ($hal == " data_analisis_drm") echo "active"; ?>">
              <i class="mdi  mdi-grid"></i><span class="nav-text">Data Analisa RM</span>
            </a>
          </li>
          <li class="nav-label">Pelaporan</li>

          <li class="mega-menu mega-menu-lg">
            <a class="has-arrow" href="javascript:void()" aria-expanded="false">
              <i class="mdi mdi-trending-up"></i><span class="nav-text">Pelaporan</span>
            </a>
            <ul aria-expanded="false">
              <li>
                <a href="{{url('data_analisis_drm_chart')}}" class="<?php if ($hal == " data_analisis_drm") echo "active"; ?>">
                  Data Kelengkapan RM
                </a>
              </li>
              <li>
                <a href="{{url('data_analisis_chart_ruang')}}" class="<?php if ($hal == " chart_ruangan") echo "active"; ?>">
                  Ruangan
                </a>
              </li>
              <li>
                <a href="{{url('data_analisis_chart_dokter')}}" class="<?php if ($hal == " chart_dokter") echo "active"; ?>">
                  Dokter
                </a>
              </li>

            </ul>
          </li>
        </ul>
      </div>
    </div>
    <!--**********************************
            Sidebar end
        ***********************************-->

    <!--**********************************
            Content body start
        ***********************************-->
    <div class="content-body">
      @yield('content')
      <!-- #/ container -->
    </div>
    <!--**********************************
            Content body end
        ***********************************-->


    <!--**********************************
            Footer start
        ***********************************-->
    <div class="footer">
      <div class="copyright">
        <p>Copyright &copy; <a href="#">Ayu Anggita</a>, 2019</p>

      </div>
    </div>
    <!--**********************************
            Footer end
        ***********************************-->


    <!--**********************************
            Right sidebar start
        ***********************************-->

    <!--**********************************
            Right sidebar end
        ***********************************-->
  </div>
  <!--**********************************
      Main wrapper end
  ***********************************-->

  <!--**********************************
      Scripts
  ***********************************-->
  <script src="{{asset('public/assets/plugins/common/common.min.js')}}"></script>
  <script src="{{asset('public/assets/js/custom.min.js')}}"></script>
  <script src="{{asset('public/assets/js/settings.js')}}"></script>
  <script src="{{asset('public/assets/js/gleek.js')}}"></script>
  <script src="{{asset('public/assets/js/styleSwitcher.js')}}"></script>


  <script src="{{asset('public/assets/plugins/moment/moment.min.js')}}"></script>
  <script src="{{asset('public/assets/plugins/datatables/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('public/assets/js/plugins-init/datatables.init.js')}}"></script>
  <script src="{{asset('public/assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
  <script src="{{asset('public/assets/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
  <script src="{{asset('public/assets/plugins/jquery-steps/build/jquery.steps.min.js')}}"></script>
  <script src="{{asset('public/assets/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
  <script src="{{asset('public/assets/js/plugins-init/jquery-steps-init.js')}}"></script>
  <script src="{{asset('public/assets/js/plugins-init/bs-date-picker-init.js')}}"></script>


  @yield('js')
  @yield('script')

</body>

</html>
