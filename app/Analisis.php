<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Analisis extends Model
{
  protected $table = 'analisis_drm';
  protected $primaryKey = 'no_analisa';
}
