<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ruang extends Model
{
    use SoftDeletes;
  protected $table = 'tb_ruangan';
  protected $primaryKey = 'id_ruang';
}
