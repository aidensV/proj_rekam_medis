<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Pasien extends Model
{
    use SoftDeletes;
  protected $table = 'tb_pasien';
  protected $primaryKey = 'no_rm';

}
