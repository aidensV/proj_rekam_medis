<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use App\Analisis;

class printDetailExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;

    public function __construct(int $year,int $month)
      {
          $this->year = $year;
          $this->month = $month;
      }

    // public function query()
    // {
    //
    //   return Analisis::query()->select('*')
    //                ->join('tb_dokter','tb_dokter.dokter_id','=','analisa_kuantitatif_resume_medis.dokter_id')
    //                ->join('tb_pasien','tb_pasien.pasien_no_rm','=','analisa_kuantitatif_resume_medis.pasien_no_rm')
    //                ->whereMonth('analis_tgl',$this->month)
    //                ->whereYear('analis_tgl',$this->year);
    //
    // }

    public function view(): View
  {
      return view('admin_assembling.data_analisis.print_excel_detail', [
          'users' => Analisis::join('tb_dokter','tb_dokter.dokter_id','=','analisa_kuantitatif_resume_medis.dokter_id')
                         ->join('tb_pasien','tb_pasien.pasien_no_rm','=','analisa_kuantitatif_resume_medis.pasien_no_rm')
                         ->whereMonth('analis_tgl',$this->month)
                         ->whereYear('analis_tgl',$this->year)->get()
      ]);
  }
  // public function view(): View
  //  {
  //      return view('exports.invoices', [
  //          'invoices' => Invoice::all()
  //      ]);
  //  }

    // public function headings(): array
    // {
    //   return [
    //      ['First row', 'First row'],
    //      ['Second row', 'Second row']
    //   ];
    // }
}
