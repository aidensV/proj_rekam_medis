<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Dokter extends Model
{
    use SoftDeletes;
  protected $table = 'tb_dokter';
  protected $primaryKey = 'id_dokter';
}
