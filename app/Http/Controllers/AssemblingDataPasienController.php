<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pasien;

class AssemblingDataPasienController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data_pasien = Pasien::orderBy('updated_at','DESC')->get();
      return view('admin_assembling.master_pasien.index',compact('data_pasien'))->with('no',1);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('admin_assembling.master_pasien.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // dd($n  oRm);

        $pasien = new Pasien;
        $pasien->no_rm =$request['pasien_no_rm'];
        $pasien->nama = $request['pasien_nama'];
        if ($request['pasien_tgl_lahir'] != null) {

          $pasien->tgl_lahir = date('Y-m-d',strtotime(str_replace('/','-',$request['pasien_tgl_lahir']))) ;
          // dd($request['pasien_tgl_lahir']);
        }else{
          $pasien->tgl_lahir = null;
        }
        $pasien->jenis_kelamin =$request['pasien_jk'] ?? null;
        $pasien->alamat = $request['pasien_alamat'] ?? null;
        // dd($request['pasien_tgl_lahir']);
        $pasien->save();

        return redirect('data_pasien');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $data = Pasien::find($id);
      return view('admin_assembling.master_pasien.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data = Pasien::find($id);
      return view('admin_assembling.master_pasien.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $pasien = Pasien::find($id);
      $pasien->no_rm =$request['pasien_no_rm'];
      $pasien->nama = $request['pasien_nama'];
      $pasien->tgl_lahir = date('Y-m-d',strtotime(str_replace('/','-',$request['pasien_tgl_lahir'])));
      $pasien->jenis_kelamin = $request['pasien_jk'];
      $pasien->alamat = $request['pasien_alamat'];
      $pasien->update();

      return redirect('data_pasien');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pasien = Pasien::find($id);

        $pasien->delete();

        return redirect('data_pasien');
    }
}
