<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ruang;

class OperatorDataRuangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $data_ruang = Ruang::orderBy('updated_at','DESC')->get();
      return view('admin_assembling.master_ruang.index',compact('data_ruang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $data_ruang = new Ruang;
      // $data_ruang->kamar_id = $request['kamar_nama'];
      $data_ruang->nama_ruang = $request['ruangan_nama'];
      $data_ruang->kelas_ruang = $request['ruangan_kelas'];
      $data_ruang->save();
      return redirect('data_ruang');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data_ruang = Ruang::find($id);
      echo json_encode($data_ruang);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $data_ruang = Ruang::find($id);
      $data_ruang->kelas_ruang = $request['ruangan_kelas'];
      $data_ruang->nama_ruang = $request['ruangan_nama'];
      $data_ruang->update();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
     {
       $data_ruang = Ruang::find($id);
       $data_ruang->delete();
     }

     public function listData()
     {
         // $data_ruang = Ruang::select('kamar_nama','ruangan_nama','ruangan_id')
         // ->join('tb_kamar','tb_ruangan.kamar_id','=','tb_kamar.kamar_id')
         // ->orderBy('tb_ruangan.updated_at', 'asc')->get();
         $data_ruang = Ruang::orderBy('updated_at','desc')->get();
         $no = 0;
         $data = array();
         foreach ($data_ruang as $list) {
             $no++;
             $row = array();
             $row[] = $no;
             $row[] = $list->kelas_ruang;
             $row[] = $list->nama_ruang;
             $row[] = '<a onclick="editForm('.$list->id_ruang.')"><button class="btn btn-warning btn-ft" ><i class="icon ion-edit"></i></button></a>
                         <a onclick="deleteData('.$list->id_ruang.')"><button class="btn btn-danger btn-ft"><i class="icon ion-trash-b"></i></button></a>';
             $data[] = $row;

         }

         $output = array("data" => $data);
         return response()->json($output);
     }
}
