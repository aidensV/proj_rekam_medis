<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\admin_assembling;
use App\admin_pelapor;
use App\User;
use Illuminate\Support\Facades\Auth;

class ChangeProfileController extends Controller
{
    public function edit_profil_user($id)
    {
        $user = User::find($id);

        return view('admin_assembling.dashboard.change_profile',compact('user'));

    }

    public function update_profil_user(Request $request, $id)
    {
            $user = User::find($id);
            if ($request->input('password') == !null) {
                $user->password = bcrypt($request->input('password'));
            }
            $user->username = $request['username'];
            $user->nama_petugas = $request['nama'];
            $user->update();
            return redirect()->route('dash_all', [Auth::user()->level]);
    }


    public function edit_profil_operator($id)
    {

        $data_o = User::find($id);

        return view('operator.dashboard.change_profile', compact('data_o'));
    }

    public function update_profil_operator(Request $request, $id)
    {
        $user = User::find($id);

        if ($request->input('password') == !null) {
            $user->password = bcrypt($request->input('password'));
        }
        $user->email = $request['email'];
        $user->name = $request['nama'];
        $user->username = $request['username'];
        $user->update();


        return redirect('dash_operator');

    }
}
