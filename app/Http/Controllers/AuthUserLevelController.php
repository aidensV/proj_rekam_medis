<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;

class AuthUserLevelController extends Controller
{
  public function index()
  {
    $level = Auth::user()->level;
    $id = Auth::user()->id;

    if ($level == "1") {
      return redirect()->route('dash_all', [$level]);
    }elseif ($level == "2") {
      return redirect()->route('dash_all', [$level]);
      // return redirect('dash_all/',$level);
    }else {
      return "anda bukan administrator";
    }
  }



}
