<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dokter;
use App\Analisis;
use DB;
use Excel;

class AssemblingChartDokterController extends Controller
{
  public function index()
  {
      $thn = date('Y');
      $bln = date('m');
      $awal=null;
      $akhir=null;
      $year = DB::table('analisis_drm')->select(DB::raw('YEAR(tgl_analisa) AS thn'))->distinct()->get();
      $chart_dokter =DB::select(DB::raw("select DISTINCT abc.nama_dokter,abc.id_dokter
,(select count(jml_data) from view_kel_dokter c where c.id_dokter  = abc.id_dokter  and c.jml_data=0) tl
,(select count(jml_data) from view_kel_dokter c where c.id_dokter  = abc.id_dokter  and c.jml_data=1) lengkap
,(select count(jml_data) from view_kel_dokter c where c.id_dokter  = abc.id_dokter  and c.jml_data=0) +(select count(jml_data) from view_kel_dokter c where c.id_dokter  = abc.id_dokter  and c.jml_data=1) as jml
from(SELECT analisis_drm.id_dokter, tb_dokter.nama_dokter, ((CASE
                            WHEN alamat IS NULL THEN 0
                            WHEN nama IS NULL THEN 0
                            WHEN tb_pasien.jenis_kelamin IS NULL THEN 0
                            WHEN tgl_lahir IS NULL THEN 0
                            ELSE 1 END)* (analisis_drm.rek_indikasi_masuk
	* analisis_drm.rek_diagnosa_masuk
	* analisis_drm.rek_prosedur
	* analisis_drm.rek_pem_fisik
	* analisis_drm.rek_pem_penunjang
	* analisis_drm.rek_tindakan_pros
	* analisis_drm.rek_kode_icd9
    * analisis_drm.rek_diagnosa_akhir
    * analisis_drm.rek_kode_icd10
    * analisis_drm.rek_pem_medika
    * analisis_drm.rek_obat_pulang
    * analisis_drm.rek_kondisi_pulang
	* analisis_drm.rek_instruksi
    * analisis_drm.rek_indikasi_pulang
    * analisis_drm.rek_tgl_kontrol
    * analisis_drm.rek_kondisi_men
	* analisis_drm.rek_transport
    * analisis_drm.aut_nama_dokter
    * analisis_drm.aut_ttd_dokter
    * analisis_drm.cat_tanggal
    * analisis_drm.cat_baris
    * analisis_drm.cat_koreksi)) as jml_data
    FROM `analisis_drm`
    INNER JOIN tb_dokter on tb_dokter.id_dokter = analisis_drm.id_dokter
    inner join `tb_pasien` on `analisis_drm`.`no_rm` = `tb_pasien`.`no_rm`
    WHERE MONTH(analisis_drm.tgl_analisa)='$bln' AND YEAR(analisis_drm.tgl_analisa)='$thn') as abc"));
          // dd($chart_dokter);

                      $avgTrueArr=array();
                      $avgFalseArr=array();
                      $data1 = array_column($chart_dokter, 'lengkap');
                      $data2 = array_column($chart_dokter, 'tl');
                      $pembagi = array_column($chart_dokter, 'jml');
                      // dd($chart_ruang);
                      $data3 = array_column($chart_dokter, 'nama_dokter');

                      for ($i=0; $i < count($chart_dokter); $i++) {
                        // print_r($data1[$i] );

                          if ($data1[$i]==0 AND $data2[$i]==0) {
                            $persenTrue = 0;
                            $persenFalse = 0;
                          }else{
                            // print_r($data1[$i] + $data2[$i]);
                            $persenTrue = round(($data1[$i]/$pembagi[$i])*100);
                            $persenFalse = round(($data2[$i]/$pembagi[$i])*100);
                          }
                          array_push($avgTrueArr,$persenTrue);
                          array_push($avgFalseArr,$persenFalse);
                        }
                        // dd($avgFalseArr);
          $awal=null;
          $akhir=null;
        $dta_dokter = Dokter::select('id_dokter','nama_dokter')->orderBy('nama_dokter','ASC')->get();
//        dd($data3);
      return view('admin_assembling.chart_analisis.data_dokter', compact('year','dta_dokter','bln','thn','awal','akhir'))
          //   // ->response()->json($query_lengkap);
          ->with('dokter', json_encode($data3))
          ->with('jmlh', json_encode($pembagi))
          ->with('jmlh_dokter', 0)
          ->with('jmlh_pasien', 0)
          ->with('total_lengkap', json_encode($avgTrueArr, JSON_NUMERIC_CHECK))
          ->with('total_tidak_lengkap', json_encode($avgFalseArr, JSON_NUMERIC_CHECK));
  }

  public function chart_kelengkapan_dokter_pertanggal(Request $request)
  {
      $awal = date('Y-m-d', strtotime(str_replace('/', '-', $request['cari_awal'])));
      $akhir = date('Y-m-d', strtotime(str_replace('/', '-', $request['cari_akhir'])));

      $year = DB::table('analisis_drm')->select(DB::raw('YEAR(tgl_analisa) AS thn'))->distinct()->get();
      $chart_dokter =DB::select(DB::raw("select DISTINCT abc.nama_dokter,abc.id_dokter
,(select count(jml_data) from view_kel_dokter c where c.id_dokter=abc.id_dokter and c.jml_data=0) tl
,(select count(jml_data) from view_kel_dokter c where c.id_dokter=abc.id_dokter and c.jml_data=1) lengkap
,(select count(jml_data) from view_kel_dokter c where c.id_dokter=abc.id_dokter and c.jml_data=0) +(select count(jml_data) from view_kel_dokter c where c.id_dokter=abc.id_dokter and c.jml_data=1) as jml
from(SELECT analisis_drm.id_dokter, tb_dokter.nama_dokter, ((CASE
                            WHEN alamat IS NULL THEN 0
                            WHEN nama IS NULL THEN 0
                            WHEN tb_pasien.jenis_kelamin IS NULL THEN 0
                            WHEN tgl_lahir IS NULL THEN 0
                            ELSE 1 END)* (analisis_drm.rek_indikasi_masuk
	* analisis_drm.rek_diagnosa_masuk
	* analisis_drm.rek_prosedur
	* analisis_drm.rek_pem_fisik
	* analisis_drm.rek_pem_penunjang
	* analisis_drm.rek_tindakan_pros
	* analisis_drm.rek_kode_icd9
    * analisis_drm.rek_diagnosa_akhir
    * analisis_drm.rek_kode_icd10
    * analisis_drm.rek_pem_medika
    * analisis_drm.rek_obat_pulang
    * analisis_drm.rek_kondisi_pulang
	* analisis_drm.rek_instruksi
    * analisis_drm.rek_indikasi_pulang
    * analisis_drm.rek_tgl_kontrol
    * analisis_drm.rek_kondisi_men
	* analisis_drm.rek_transport
    * analisis_drm.aut_nama_dokter
    * analisis_drm.aut_ttd_dokter
    * analisis_drm.cat_tanggal
    * analisis_drm.cat_baris
    * analisis_drm.cat_koreksi)) as jml_data
    FROM `analisis_drm`
    INNER JOIN tb_dokter on tb_dokter.id_dokter = analisis_drm.id_dokter
    inner join `tb_pasien` on `analisis_drm`.`no_rm` = `tb_pasien`.`no_rm`
    where (analisis_drm.tgl_analisa >= '$awal' and analisis_drm.tgl_analisa <= '$akhir')) as abc"));
          // dd($chart_dokter);

          $avgTrueArr=array();
          $avgFalseArr=array();
          $data1 = array_column($chart_dokter, 'lengkap');
          $data2 = array_column($chart_dokter, 'tl');
          $pembagi = array_column($chart_dokter, 'jml');
          // dd($chart_ruang);
          $data3 = array_column($chart_dokter, 'nama_dokter');

          for ($i=0; $i < count($chart_dokter); $i++) {
            // print_r($data1[$i] );

              if ($data1[$i]==0 AND $data2[$i]==0) {
                $persenTrue = 0;
                $persenFalse = 0;
              }else{
                // print_r($data1[$i] + $data2[$i]);
                $persenTrue = round(($data1[$i]/$pembagi[$i])*100);
                $persenFalse = round(($data2[$i]/$pembagi[$i])*100);
              }
              array_push($avgTrueArr,$persenTrue);
              array_push($avgFalseArr,$persenFalse);
            }
            $nama_dokter=NULL;
  $dta_dokter = Dokter::select('id_dokter','nama_dokter')->orderBy('nama_dokter','ASC')->get();
//        dd($data3);
      return view('admin_assembling.chart_analisis.data_dokter', compact('year','awal','akhir','dta_dokter','nama_dokter'))
          //   // ->response()->json($query_lengkap);
          ->with('dokter', json_encode($data3))
          ->with('jmlh', json_encode($pembagi))
          ->with('jmlh_dokter', 0)
          ->with('jmlh_pasien', 0)
          ->with('total_lengkap', json_encode($avgTrueArr, JSON_NUMERIC_CHECK))
          ->with('total_tidak_lengkap', json_encode($avgFalseArr, JSON_NUMERIC_CHECK));
  }
  public function chart_kelengkapan_dokter_perdokter(Request $request)
  {
      $awal = date('Y-m-d', strtotime(str_replace('/', '-', $request['cari_awal'])));
      $akhir = date('Y-m-d', strtotime(str_replace('/', '-', $request['cari_akhir'])));
      $id_dokter = $request['dokter_nama'];

      $pasien_l = Analisis::join('tb_pasien','tb_pasien.no_rm','=','analisis_drm.no_rm')
                                ->where('analisis_drm.id_dokter',$id_dokter)
                                ->whereNotNull('tgl_lahir')
                                ->whereNotNull('tb_pasien.jenis_kelamin')
                                ->whereNotNull('alamat')
                                ->whereBetween('tgl_analisa',[$awal,$akhir])->count();
      $pasien_tl = Analisis::join('tb_pasien','tb_pasien.no_rm','=','analisis_drm.no_rm')
                                ->where('analisis_drm.id_dokter',$id_dokter)
                                ->whereNull('tgl_lahir')
                                ->WhereNull('tb_pasien.jenis_kelamin')
                                ->WhereNull('alamat')
                                ->whereBetween('tgl_analisa',[$awal,$akhir])
                                ->count();
                                // dd($pasien_l);

      $records = DB::table('analisis_drm')
      ->join('tb_pasien','tb_pasien.no_rm','=','analisis_drm.no_rm')
      ->join('tb_dokter','tb_dokter.id_dokter','=','analisis_drm.id_dokter')
      ->join('tb_ruangan','tb_ruangan.id_ruang','=','analisis_drm.id_ruang')
      ->where('analisis_drm.id_dokter',$id_dokter)
      ->whereBetween('tgl_analisa',[$awal,$akhir])->get();

      $columns = ['rek_indikasi_masuk', 'rek_diagnosa_masuk',
          'rek_prosedur', 'rek_pem_fisik', 'rek_pem_penunjang', 'rek_tindakan_pros', 'rek_kode_icd9',
          'rek_diagnosa_akhir', 'rek_kode_icd10', 'rek_pem_medika', 'rek_obat_pulang', 'rek_kondisi_pulang',
          'rek_instruksi','rek_indikasi_pulang', 'rek_tgl_kontrol', 'rek_kondisi_men', 'rek_transport', 'aut_nama_dokter',
           'aut_ttd_dokter', 'cat_tanggal', 'cat_baris','cat_koreksi'];
      $data = [];
      $jumlahdata="";
      foreach ($columns as $key=> $column) {
          $data[] = [
              'nama_kolom' => $column,
              'jumlah' => $records->count(),
              // 'jumlah' => ($key == 0)? $jumlahdata=$column : $jumlahdata *= $column,
              'count_true' => $records->where($column, 1)->count(),
              'count_false' => $records->where($column, 0)->count()
          ];
      }
      if ($pasien_l == 0 AND $pasien_tl == 0) {
        $pasien_avg_l = 0;
        $pasien_avg_tl = 0;
      }else {
        $pasien_avg_l = round($pasien_l / ($pasien_l+$pasien_tl) * 100);
        $pasien_avg_tl = round($pasien_tl / ($pasien_l+$pasien_tl) * 100);
      }
      $avgTrueArr=array();
      $avgFalseArr=array();
      array_push($avgTrueArr,$pasien_avg_l);
      array_push($avgFalseArr,$pasien_avg_tl);
      $data_kolom = array_column($data, 'jumlah');
      $data_true = array_column($data, 'count_true');
      $data_false = array_column($data, 'count_false');
      $jml_data=$data_kolom[0];
      $jml_pasien = $pasien_l + $pasien_tl;
      $jml_data_pasien=$pasien_l+$pasien_tl;
      for ($i=0; $i < count($data_kolom); $i++) {
          if ($data_true[$i]==0 AND $data_false[$i]==0) {
            $persenTrue = 0;
            $persenFalse = 0;
          }else{
            $persenTrue = round(($data_true[$i]/$data_kolom[$i])*100);
            $persenFalse = round(($data_false[$i]/$data_kolom[$i])*100);
          }
          array_push($avgTrueArr,$persenTrue);
          array_push($avgFalseArr,$persenFalse);
        }
        $nama_dokter='';
        $id_dokter='';
        foreach ($records as $key => $value) {
          $nama_dokter=$value->nama_dokter;
          $id_dokter=$value->id_dokter;
        }

        $data3 =array("Ident","Ind Px", "dx Msk", "Prosedur", "Fisik", "Penunjang", "Tind Pros", "Icd 9", "Dx Akhir", "Icd 10", "Medika",
        "Obat Plg", "knd plg", "Inst",
         "Indikasi", "Tgl Ktrl", "Knd Mndsk", "Trans","Nama Dok","Ttd Dok","Baris","Korksi","tgl" );

        $dta_dokter = Dokter::select('id_dokter','nama_dokter')->orderBy('nama_dokter','ASC')->get();
      return view('admin_assembling.chart_analisis.data_dokter', compact('year','dta_dokter','nama_dokter','id_dokter','awal','akhir'))
          //   // ->response()->json($query_lengkap);
          ->with('dokter', json_encode($data3))
          ->with('jmlh_dokter', json_encode($jml_data))
          ->with('jmlh_pasien', json_encode($jml_pasien))
          ->with('jmlh', 1)
          ->with('total_lengkap', json_encode($avgTrueArr, JSON_NUMERIC_CHECK))
          ->with('total_tidak_lengkap', json_encode($avgFalseArr, JSON_NUMERIC_CHECK));
  }
  public function print_dokter($awal,$akhir,$id)
  {
    $bln = null;
    $thn = null;

    $dt = Analisis::select('*',DB::raw('(rek_indikasi_masuk * rek_diagnosa_masuk * rek_prosedur * rek_pem_fisik * rek_pem_penunjang * rek_tindakan_pros
                    * rek_kode_icd9 * rek_diagnosa_akhir * rek_kode_icd10 * rek_pem_medika * rek_obat_pulang * rek_kondisi_pulang
                     * rek_instruksi * rek_indikasi_pulang * rek_tgl_kontrol * rek_kondisi_men
                      * rek_transport * aut_nama_dokter * aut_ttd_dokter * cat_tanggal * cat_baris * cat_koreksi) as status')
                      )
                  ->join('tb_dokter','tb_dokter.id_dokter','=','analisis_drm.id_dokter')
                  ->join('tb_pasien','tb_pasien.no_rm','=','analisis_drm.no_rm')
                  ->join('tb_ruangan','tb_ruangan.id_ruang','=','analisis_drm.id_ruang')
                  ->where('analisis_drm.id_dokter',$id)
                  ->whereBetween('tgl_analisa',[$awal,$akhir])
                  ->get();
                  // dd($dt);
                  $nama_dokter = Dokter::where('id_dokter',$id)->value('nama_dokter');

                  // dd($dt);
        return Excel::create('Lap_Dokter - '.$awal.$akhir, function($excel) use ($dt,$bln,$thn,$awal,$akhir,$nama_dokter) {

              $excel->sheet('Resume Medis', function($sheet) use ($dt,$bln,$thn,$awal,$akhir,$nama_dokter) {

                  $sheet->loadView('admin_assembling.chart_analisis.print_excel_dokter',compact('dt','bln','thn','awal','akhir','nama_dokter'))->with('no',1);

              });

          })->download('xls');
  }
}
