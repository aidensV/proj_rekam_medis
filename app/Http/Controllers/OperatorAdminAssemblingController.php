<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin_assembling;
use App\User;

class OperatorAdminAssemblingController extends Controller
{

    public function index()
    {
      $admin_assembling = User::where('level','2')
      ->orderBy('updated_at', 'desc')
      ->get();

      return view ('admin_assembling.admin_assembling.index',compact('admin_assembling'))->with('no',1);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin_assembling.admin_assembling.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


          $user = new User;

          $user->nama_petugas = $request['admin_assembling_nama'];
          $user->username = $request['username'];
          $user->password = bcrypt($request['password']);
          $user->level = "2";
          $user->save();
          //
          // $users_id_get = User::MAX('id')->get();
          //
          // foreach ($users_id_get as $key) {
          //   $users_id = $key->id;
          // }
          //
          // $admin_assembling = new Admin_assembling;
          // $admin_assembling->users_id = $users_id;
          // $admin_assembling->admin_assembling_nama = $request['admin_assembling_nama'];
          // $admin_assembling->admin_assembling_nohp = $request['admin_assembling_nohp'];
          // $admin_assembling->admin_assembling_alamat = $request['admin_assembling_alamat'];
          // $admin_assembling->save();

          return redirect('admin_assembling');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data = User::find($id);
      return view('admin_assembling.admin_assembling.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $admin_assembling = User::find($id);
      $admin_assembling->nama_petugas = $request['admin_assembling_nama'];
      $admin_assembling->username = $request['admin_assembling_username'];
      if ($request['admin_assembling_password'] == !null) {
        $admin_assembling->password = bcrypt($request['admin_assembling_password']);
      }
      $admin_assembling->update();

      return redirect('admin_assembling');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $user = User::find($id);
      $user->delete();
      return back();
    }
}
