<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pasien;
use App\Analisis;
use DB;
use Excel;
use Carbon;
class AssemblingChartDrmController extends Controller
{

    // public function index()
    // {
    //   $thn = date('Y');
    //   $bln = date('m');
    //   $pasien_l = Pasien::join('analisis_drm','tb_pasien.no_rm','=','analisis_drm.no_rm')
    //                             ->whereNotNull('tgl_lahir')
    //                             ->whereNotNull('jenis_kelamin')
    //                             ->whereNotNull('alamat')
    //                             ->whereMonth('tgl_analisa', $bln)
    //                             ->whereYear('tgl_analisa', $thn)->count();
    //   $pasien_tl = Pasien::join('analisis_drm','tb_pasien.no_rm','=','analisis_drm.no_rm')
    //                             ->whereNull('tgl_lahir')
    //                             ->orWhereNull('jenis_kelamin')
    //                             ->orWhereNull('alamat')
    //                             ->whereMonth('tgl_analisa', $bln)
    //                             ->whereYear('tgl_analisa', $thn)->count();
    //                             // dd($pasien_tl);
    //   $records = DB::table('analisis_drm')
    //       ->join('tb_pasien','tb_pasien.no_rm','=','analisis_drm.no_rm')
    //       ->join('tb_dokter','tb_dokter.id_dokter','=','analisis_drm.id_dokter')
    //       ->whereMonth('tgl_analisa', $bln)
    //       ->whereYear('tgl_analisa', $thn)->get();
    //       // dd($records);
    //   $columns = ['rek_indikasi_masuk', 'rek_diagnosa_masuk',
    //       'rek_prosedur', 'rek_pem_fisik', 'rek_pem_penunjang', 'rek_tindakan_pros', 'rek_kode_icd9',
    //       'rek_diagnosa_akhir', 'rek_kode_icd10', 'rek_pem_medika', 'rek_obat_pulang', 'rek_kondisi_pulang',
    //       'rek_instruksi','rek_indikasi_pulang', 'rek_tgl_kontrol', 'rek_kondisi_men', 'rek_transport', 'aut_nama_dokter',
    //        'aut_ttd_dokter', 'cat_tanggal', 'cat_baris','cat_koreksi'];
    //   $data = [];
    //   $data2 = [];
    //   // $jumlahdata="";
    //   foreach ($columns as $key=> $column) {
    //       $data[] = [
    //           'kolom' => $column,
    //           // 'select_sum' => $records->select('sum('.$columns.')')->get(),
    //           'count_true' => $records->where($column, 1)->count(),
    //           'jumlah' => $records->count(),
    //           'count_false' => $records->where($column, 0)->count()
    //       ];
    //   }
    //   // dd($pasien_avg_tl);
    //   // $hasil = [];
    //   // foreach ($records as $key => $value) {
    //   //   $datas = 1;
    //   //   foreach ($columns as $keys => $values) {
    //   //     $datas *= $value->$values;
    //   //   }
    //   //   $hasil[]=$datas;
    //   // }
    //
    //   // $select_kolom = array_column($data,'select_sum');
    //   // dd($pasien_tl);
    //   if ($pasien_l == 0 AND $pasien_tl == 0) {
    //     $pasien_avg_l = 0;
    //     $pasien_avg_tl = 0;
    //   }else {
    //     $pasien_avg_l = round($pasien_l / ($pasien_l+$pasien_tl) * 100);
    //     $pasien_avg_tl = round($pasien_tl / ($pasien_l+$pasien_tl) * 100);
    //   }
    //   $data_kolom = array_column($data,'jumlah');
    //   // dd($select_kolom);
    //   $avgTrueArr = array();
    //   $avgFalseArr = array();
    //   array_push($avgTrueArr,$pasien_avg_l);
    //   array_push($avgFalseArr,$pasien_avg_tl);
    //   $data_true = array_column($data, 'count_true');
    //   $data_false = array_column($data, 'count_false');
    //   $jml_data=$data_kolom[0];
    //   $jml_data_pasien=$pasien_l+$pasien_tl;
    //   // dd($jml_data);
    //   for ($i=0; $i < count($data_kolom); $i++) {
    //     if ($data_true[$i]==0 AND $data_false[$i]==0) {
    //       $persenTrue = 0;
    //       $persenFalse = 0;
    //     }else{
    //       $persenTrue = round(($data_true[$i]/$data_kolom[$i])*100);
    //       $persenFalse = round(($data_false[$i]/$data_kolom[$i])*100);
    //     }
    //     array_push($avgTrueArr,$persenTrue);
    //     array_push($avgFalseArr,$persenFalse);
    //   }
    //   // dd($avgTrueArr);
    //   // if (count($records) < 0) {
    //   //   $chart_l_avg = (array_sum($data_kolom) / $data_true) * 100;
    //   //   $chart_tl_avg = (array_sum($data_kolom) / $data_false) * 100;
    //   // }else {
    //   //   $chart_l_avg = 0;
    //   //   $chart_tl_avg = 0;
    //   // }
    //   $awal=null;
    //   $akhir=null;
    //   // dd($data_kolom);
    //
    //   return view('admin_assembling.chart_analisis.data_analisis', compact('year','bln','thn','awal','akhir'))
    //       //   // ->response()->json($query_lengkap);
    //       // ->with('lengkap', json_encode($chart_l_avg, JSON_NUMERIC_CHECK))
    //       ->with('lengkap_avg', json_encode($avgTrueArr, JSON_NUMERIC_CHECK))
    //       ->with('no', 1)
    //       ->with('jumlah', json_encode($jml_data, JSON_NUMERIC_CHECK))
    //       ->with('jumlah_pasien', json_encode($jml_data_pasien, JSON_NUMERIC_CHECK))
    //       // ->with('tidak_lengkap', json_encode($chart_tl_avg, JSON_NUMERIC_CHECK))
    //       ->with('tidak_lengkap_avg', json_encode($avgFalseArr, JSON_NUMERIC_CHECK));
    // }
    public function index()
    {
      $thn = date('Y');
      $bln = date('m');
      $awal = null;
      $akhir = null;
      $jml_data_pasien=0;

      // $item_bulan2 = DB::table('view_item')->get()->toArray();
      // dd($item_bulan2);
      $item_bulan = DB::select(DB::raw("select distinct bln,
	(select count(jml_data) from view_item c where c.bln COLLATE utf8mb4_general_ci = abc.bln COLLATE utf8mb4_general_ci and c.jml_data=0) tl,
	(select count(jml_data) from view_item c where c.bln COLLATE utf8mb4_general_ci = abc.bln COLLATE utf8mb4_general_ci and c.jml_data=1) lengkap,
    (select count(jml_data) from view_item c where c.bln COLLATE utf8mb4_general_ci = abc.bln COLLATE utf8mb4_general_ci and c.jml_data=0) +
    (select count(jml_data) from view_item c where c.bln COLLATE utf8mb4_general_ci = abc.bln COLLATE utf8mb4_general_ci and c.jml_data=1) jml
    from (SELECT CONCAT(CASE MONTH(tgl_analisa)
    WHEN 1 THEN 'Januari'
    WHEN 2 THEN 'Februari'
    WHEN 3 THEN 'Maret'
    WHEN 4 THEN 'April'
    WHEN 5 THEN 'Mei'
    WHEN 6 THEN 'Juni'
    WHEN 7 THEN 'Juli'
    WHEN 8 THEN 'Agustus'
    WHEN 9 THEN 'September'
    WHEN 10 THEN 'Oktober'
    WHEN 11 THEN 'November'
    WHEN 12 THEN 'Desember'
  END) as bln,
		((CASE
			WHEN alamat IS NULL THEN 0
			WHEN nama IS NULL THEN 0
			WHEN jenis_kelamin IS NULL THEN 0
			WHEN tgl_lahir IS NULL THEN 0
			ELSE 1 END) *
		(rek_indikasi_masuk * rek_diagnosa_masuk * rek_prosedur * rek_pem_fisik * rek_pem_penunjang * rek_tindakan_pros
		* rek_kode_icd9 * rek_diagnosa_akhir * rek_kode_icd10 * rek_pem_medika * rek_obat_pulang * rek_kondisi_pulang
		* rek_instruksi * rek_indikasi_pulang * rek_tgl_kontrol * rek_kondisi_men
		* rek_transport * aut_nama_dokter * aut_ttd_dokter * cat_tanggal * cat_baris * cat_koreksi) ) as jml_data
FROM `analisis_drm`
inner join `tb_pasien` on `analisis_drm`.`no_rm` = `tb_pasien`.`no_rm`) abc "));
// dd($item_bulan);
      $tl = array_column($item_bulan,'tl');
      $l = array_column($item_bulan,'lengkap');
      $nama_bln = array_column($item_bulan,'bln');
      $jmlh = array_column($item_bulan,'jml');
      $avg_l = array();
      $avg_tl = array();
      for ($i=0; $i < count($item_bulan); $i++) {
        if ($l[$i] <= 0 AND $tl[$i] <= 0) {
          $avg_l = 0;
          $avg_tl = 0;
        }else {
          $op_l = round(($l[$i]/$jmlh[$i]) * 100);
          $op_tl = round(($tl[$i]/$jmlh[$i]) * 100);
        }
        array_push($avg_l,$op_l);
        array_push($avg_tl,$op_tl);
      }
      // dd($bln);
      return view('admin_assembling.chart_analisis.data_analisis2', compact('thn','bln','awal','akhir'))
            //   // ->response()->json($query_lengkap);
            // ->with('lengkap', json_encode($chart_l_avg, JSON_NUMERIC_CHECK))
            ->with('lengkap_avg', json_encode($avg_l, JSON_NUMERIC_CHECK))
            ->with('no', 1)
            ->with('nama_bulan',json_encode($nama_bln))
            ->with('jumlah', json_encode($jmlh, JSON_NUMERIC_CHECK))
            ->with('jumlah_pasien', json_encode($jml_data_pasien, JSON_NUMERIC_CHECK))
            // ->with('tidak_lengkap', json_encode($chart_tl_avg, JSON_NUMERIC_CHECK))
            ->with('tidak_lengkap_avg', json_encode($avg_tl, JSON_NUMERIC_CHECK));

    }
    public function chart_kelengkapan_drm_perbulan(Request $request)
    {
        $awal = date('Y-m-d', strtotime(str_replace('/', '-', $request['cari_awal'])));
        $akhir = date('Y-m-d', strtotime(str_replace('/', '-', $request['cari_akhir'])));

        $pasien_l = Pasien::join('analisis_drm','tb_pasien.no_rm','=','analisis_drm.no_rm')
                                  ->whereNotNull('tgl_lahir')
                                  ->whereNotNull('jenis_kelamin')
                                  ->whereNotNull('alamat')
                                  ->whereBetween('tgl_analisa',[$awal,$akhir])->count();
        $pasien_tl = Pasien::join('analisis_drm','tb_pasien.no_rm','=','analisis_drm.no_rm')
                                  ->whereNull('tgl_lahir')
                                  ->orWhereNull('jenis_kelamin')
                                  ->orWhereNull('alamat')
                                  ->whereBetween('tgl_analisa',[$awal,$akhir])->count();
        //
        // $data_detail = Analisis::join('tb_pasien','tb_pasien.no_rm','=','analisis_drm.no_rm')
        //             ->whereBetween('tgl_analisa',[$awal,$akhir])
        //              ->get();

        $records = DB::table('analisis_drm')
        ->join('tb_pasien','tb_pasien.no_rm','=','analisis_drm.no_rm')
        ->join('tb_dokter','tb_dokter.id_dokter','=','analisis_drm.id_dokter')
        ->whereBetween('tgl_analisa',[$awal,$akhir])->get();
        // dd($records);
        $columns = ['rek_indikasi_masuk', 'rek_diagnosa_masuk',
            'rek_prosedur', 'rek_pem_fisik', 'rek_pem_penunjang', 'rek_tindakan_pros', 'rek_kode_icd9',
            'rek_diagnosa_akhir', 'rek_kode_icd10', 'rek_pem_medika', 'rek_obat_pulang', 'rek_kondisi_pulang',
            'rek_instruksi','rek_indikasi_pulang', 'rek_tgl_kontrol', 'rek_kondisi_men', 'rek_transport', 'aut_nama_dokter',
             'aut_ttd_dokter', 'cat_tanggal', 'cat_baris','cat_koreksi'];
        $data = [];
        $jumlahdata="";
        foreach ($columns as $key=> $column) {
            $data[] = [
                'nama_kolom' => $column,
                'jumlah' => $records->count(),
                // 'jumlah' => ($key == 0)? $jumlahdata=$column : $jumlahdata *= $column,
                'count_true' => $records->where($column, 1)->count(),
                'count_false' => $records->where($column, 0)->count()
            ];
        }
        if ($pasien_l == 0 AND $pasien_tl == 0) {
          $pasien_avg_l = 0;
          $pasien_avg_tl = 0;
        }else {
          $pasien_avg_l = round($pasien_l / ($pasien_l+$pasien_tl) * 100);
          $pasien_avg_tl = round($pasien_tl / ($pasien_l+$pasien_tl) * 100);
        }
        $avgTrueArr=array();
        $avgFalseArr=array();
        array_push($avgTrueArr,$pasien_avg_l);
        array_push($avgFalseArr,$pasien_avg_tl);
        $data_kolom = array_column($data, 'jumlah');
        $data_true = array_column($data, 'count_true');
        $data_false = array_column($data, 'count_false');
        $jml_data=$data_kolom[0];
        $jml_data_pasien=$pasien_l+$pasien_tl;
        for ($i=0; $i < count($data_kolom); $i++) {
            if ($data_true[$i]==0 AND $data_false[$i]==0) {
              $persenTrue = 0;
              $persenFalse = 0;
            }else{
              $persenTrue = round(($data_true[$i]/$data_kolom[$i])*100);
              $persenFalse = round(($data_false[$i]/$data_kolom[$i])*100);
            }
            array_push($avgTrueArr,$persenTrue);
            array_push($avgFalseArr,$persenFalse);
          }

        return view('admin_assembling.chart_analisis.data_analisis2', compact('year','data_detail','awal','akhir'))
            //   // ->response()->json($query_lengkap);
            // ->with('no', 1)
            // ->with('lengkap', json_encode($data1, JSON_NUMERIC_CHECK))
            // ->with('tidak_lengkap', json_encode($data2, JSON_NUMERIC_CHECK));
            ->with('lengkap_avg', json_encode($avgTrueArr, JSON_NUMERIC_CHECK))
            ->with('no', 1)
            ->with('nama_bulan', 0)
            ->with('jumlah', json_encode($jml_data, JSON_NUMERIC_CHECK))
            ->with('jumlah_pasien', json_encode($jml_data_pasien, JSON_NUMERIC_CHECK))
            // ->with('tidak_lengkap', json_encode($chart_tl_avg, JSON_NUMERIC_CHECK))
            ->with('tidak_lengkap_avg', json_encode($avgFalseArr, JSON_NUMERIC_CHECK));
    }
    public function print_detail_drm($bln,$thn)
    {
      $awal = null;
      $akhir = null;
      $dt = Analisis::
      select('*',DB::raw('(rek_indikasi_masuk * rek_diagnosa_masuk * rek_prosedur * rek_pem_fisik * rek_pem_penunjang * rek_tindakan_pros
                    * rek_kode_icd9 * rek_diagnosa_akhir * rek_kode_icd10 * rek_pem_medika * rek_obat_pulang * rek_kondisi_pulang
                     * rek_instruksi * rek_indikasi_pulang * rek_tgl_kontrol * rek_kondisi_men
                      * rek_transport * aut_nama_dokter * aut_ttd_dokter * cat_tanggal * cat_baris * cat_koreksi) as status')
                      )
                    ->join('tb_dokter','tb_dokter.id_dokter','=','analisis_drm.id_dokter')
                    ->join('tb_pasien','tb_pasien.no_rm','=','analisis_drm.no_rm')
                    ->join('tb_ruangan','tb_ruangan.id_ruang','=','analisis_drm.id_ruang')
                    ->whereMonth('tgl_analisa',$bln)
                    ->whereYear('tgl_analisa',$thn)
                    ->get();
              $jml_pasien = Analisis::join('tb_pasien','tb_pasien.no_rm','=','analisis_drm.no_rm')->distinct('analisis_drm.no_rm')->count('analisis_drm.no_rm');
              // dd($dt);
                    // return view('admin_assembling.data_analisis.print_excel_detail',compact('dt','bln','thn','awal','akhir','jml_pasien'))->with('no',1);
    return Excel::create('Lap_per_item - '.$bln.$thn, function($excel) use ($dt,$bln,$thn,$awal,$akhir,$jml_pasien) {

          $excel->sheet('Lap_per_item', function($sheet) use ($dt,$bln,$thn,$awal,$akhir,$jml_pasien) {

              $sheet->loadView('admin_assembling.data_analisis.print_excel_item',compact('dt','bln','thn','awal','akhir','jml_pasien'))->with('no',1);

          });

      })->download('xls');
    }

    public function print_detail_drm_cari($awal,$akhir)
    {

$bln = null;
$thn = null;

$dt = Analisis::select('*',DB::raw('(rek_indikasi_masuk * rek_diagnosa_masuk * rek_prosedur * rek_pem_fisik * rek_pem_penunjang * rek_tindakan_pros
                * rek_kode_icd9 * rek_diagnosa_akhir * rek_kode_icd10 * rek_pem_medika * rek_obat_pulang * rek_kondisi_pulang
                 * rek_instruksi * rek_indikasi_pulang * rek_tgl_kontrol * rek_kondisi_men
                  * rek_transport * aut_nama_dokter * aut_ttd_dokter * cat_tanggal * cat_baris * cat_koreksi) as status')
                  )
              ->join('tb_dokter','tb_dokter.id_dokter','=','analisis_drm.id_dokter')
              ->join('tb_pasien','tb_pasien.no_rm','=','analisis_drm.no_rm')
              ->join('tb_ruangan','tb_ruangan.id_ruang','=','analisis_drm.id_ruang')
              ->whereBetween('tgl_analisa',[$awal,$akhir])
              ->get();
              $jml_pasien = Analisis::join('tb_pasien','tb_pasien.no_rm','=','analisis_drm.no_rm')->distinct('analisis_drm.no_rm')->count('analisis_drm.no_rm');

              // dd($dt);
    return Excel::create('Lap_analisis - '.$awal.$akhir, function($excel) use ($dt,$bln,$thn,$awal,$akhir,$jml_pasien) {

          $excel->sheet('Resume Medis', function($sheet) use ($dt,$bln,$thn,$awal,$akhir,$jml_pasien) {

              $sheet->loadView('admin_assembling.data_analisis.print_excel_item',compact('dt','bln','thn','awal','akhir','jml_pasien'))->with('no',1);

          });

      })->download('xls');
    }
}
