<?php

namespace App\Http\Controllers;


use App\Analisis;
use DB;
use App\Dokter;
use App\Ruang;
use App\admin_pelapor;
use App\Admin_assembling;

class DashboardController extends Controller
{

    public function index_dash_all($level)
    {
//=============================chart total centng======================================================
        $bln = date('m');
        $thn = date('Y');
        $records = DB::table('analisis_drm')
            ->whereMonth('tgl_analisa',$bln)
            ->whereYear('tgl_analisa', $thn)
            ->get();

        $columns = ['rek_indikasi_masuk', 'rek_diagnosa_masuk',
            'rek_prosedur', 'rek_pem_fisik', 'rek_pem_penunjang', 'rek_tindakan_pros', 'rek_kode_icd9',
            'rek_diagnosa_akhir', 'rek_kode_icd10', 'rek_pem_medika', 'rek_obat_pulang', 'rek_kondisi_pulang',
            'rek_instruksi','rek_indikasi_pulang', 'rek_tgl_kontrol', 'rek_kondisi_men', 'rek_transport', 'aut_nama_dokter', 'aut_ttd_dokter', 'cat_tanggal', 'cat_baris','cat_koreksi'];
        $data = [];

        foreach ($columns as $column) {
            $data[] = [

                'column' => $column,
                'count_true' => $records->where($column, 1)->count(),
                'count_false' => $records->where($column, 0)->count(),
                'jumlah' => $records->count($column)
            ];
        }

//         =====================================END============================================

//===============================Ruangan Tidak Lengkap====================================
        $ruang_tl = Analisis::join('tb_ruangan', 'analisis_drm.id_ruang', '=', 'tb_ruangan.id_ruang')
            ->whereMonth('tgl_analisa', $bln)
            ->whereYear('tgl_analisa', $thn)
            ->groupBy('analisis_drm.id_ruang')->orderBy('total_tidak_lengkap', 'DESC')
            ->select(
                'nama_ruang',
                DB::raw('(SUM(rek_indikasi_masuk=0) + SUM(rek_diagnosa_masuk=0)+ SUM(rek_prosedur=0)+ SUM(rek_pem_fisik=0)+
  SUM(rek_pem_penunjang=0)+ SUM(rek_tindakan_pros=0)+ SUM(rek_kode_icd9=0)+ SUM(rek_diagnosa_akhir=0)+
  SUM(rek_kode_icd10=0)+ SUM(rek_pem_medika=0)+ SUM(rek_obat_pulang=0)+ SUM(rek_kondisi_pulang=0)+
  SUM(rek_instruksi=0) +SUM(rek_indikasi_pulang)+ SUM(rek_tgl_kontrol=0)+SUM(rek_kondisi_men=0)+ SUM(rek_transport=0)+
  SUM(aut_nama_dokter=0)+ SUM(aut_ttd_dokter=0)+ SUM(cat_tanggal=0)+ SUM(cat_baris=0)+ SUM(cat_koreksi=0)) AS total_tidak_lengkap')
            )->limit('1')->value('nama_ruang');

//         =====================================END============================================
//=======================PERSENTASE=====================================================
$persentase = DB::table('analisis_drm')
->join('tb_pasien','analisis_drm.no_rm','=','tb_pasien.no_rm')
->whereMonth('tgl_analisa',$bln)
->whereYear('tgl_analisa', $thn)
->select(DB::raw("((CASE
                        WHEN alamat IS NULL THEN 0
                        WHEN nama IS NULL THEN 0
                        WHEN jenis_kelamin IS NULL THEN 0
                        WHEN tgl_lahir IS NULL THEN 0
                        ELSE 1 END) * (rek_indikasi_masuk * rek_diagnosa_masuk * rek_prosedur * rek_pem_fisik * rek_pem_penunjang * rek_tindakan_pros
               * rek_kode_icd9 * rek_diagnosa_akhir * rek_kode_icd10 * rek_pem_medika * rek_obat_pulang * rek_kondisi_pulang
                * rek_instruksi * rek_indikasi_pulang * rek_tgl_kontrol * rek_kondisi_men
                 * rek_transport * aut_nama_dokter * aut_ttd_dokter * cat_tanggal * cat_baris * cat_koreksi) )as jml_data"))->get();
                 $kolomPers = ['jml_data'];
                 $dataPers = [];

                 foreach ($kolomPers as $key => $col) {
                   $dataPers [] = [
                     'jml_data_benar' => $persentase->where($col, 1)->count(),
                     'jml_data_salah' => $persentase->where($col, 0)->count()
                   ];
                 }
                 $jml_data_benar = array_column($dataPers,'jml_data_benar');
                 $jml_data_salah = array_column($dataPers,'jml_data_salah');
                 $jml_data = $jml_data_benar[0]+$jml_data_salah[0];
                 $data2 = array_unique(array_column($data, 'jumlah'));
                 $data3 = array_sum(array_column($data, 'count_true'));
                 $data4 = array_column($data, 'count_false');
                //  dd($jml_data);
                //  if ($jml_data > 0) {
                //      if ($jml_data_benar[0] < 1) {
                //         $chart_tl_avg = ($jml_data_salah[0] / $jml_data) * 100;
                //         $chart_l_avg = 0;
                //      }elseif($jml_data_salah[0] < 1){
                //         $chart_l_avg = ($jml_data_benar[0] / $jml_data) * 100;
                //         $chart_tl_avg = 0;
                //      }else{
                //         $chart_l_avg = ($jml_data_benar[0] / $jml_data) * 100;
                //         $chart_tl_avg = ($jml_data_salah[0] / $jml_data) * 100;
                //      }
                //  }else{
                //     $chart_tl_avg = 0;
                //     $chart_l_avg = 0;
                //  }
        if (count($records) > 0){
        //   if ($data3 < 1) {
        //     // dd($data3);
        //     $chart_tl_avg = (array_sum($data2) / $data4) * 100;
        //     $chart_l_avg=0;
        //   }elseif ($data4 < 1) {
        //     $chart_tl_avg = 0;
        //     $chart_l_avg = (array_sum($data2) / $data3) * 100;
        //     // code...
        //   }else{
        //     $chart_l_avg = (array_sum($data2) / $data3) * 100;
        //     $chart_tl_avg = (array_sum($data2) / $data4) * 100;
        //     // code...
        //   }
        if ($jml_data_benar[0] < 1) {
            $chart_tl_avg = ($jml_data_salah[0] / $jml_data) * 100;
            $chart_l_avg = 0;
         }elseif($jml_data_salah[0] < 1){
            $chart_l_avg = ($jml_data_benar[0] / $jml_data) * 100;
            $chart_tl_avg = 0;
         }else{
            $chart_l_avg = ($jml_data_benar[0] / $jml_data) * 100;
            $chart_tl_avg = ($jml_data_salah[0] / $jml_data) * 100;
         }

          $collection = collect($data);
          $filtered = $collection->filter();
          $dt_srt=$filtered->sortByDesc('count_false')->first();
          $nmCol=$dt_srt['column'];
          // dd($filtered);
          switch ($nmCol){
              case 'rek_indikasi_masuk':
                  $item_tidak_lengkap='Indikasi Masuk';
                  break;
              case 'rek_diagnosa_masuk':
                  $item_tidak_lengkap='Diagnosa Masuk';
                  break;
              case 'rek_prosedur':
                  $item_tidak_lengkap='Prosedur';
                  break;
              case 'rek_pem_fisik':
                  $item_tidak_lengkap='Pemeriksaan Fisik';
                  break;
              case 'rek_pem_penunjang':
                  $item_tidak_lengkap='Pemeriksaan Penunjang';
                  break;
              case 'rek_tindakan_pros':
                  $item_tidak_lengkap='Tindakan';
                  break;
              case 'rek_kode_icd9':
                  $item_tidak_lengkap='Kode ICD 9';
                  break;
              case 'rek_diagnosa_akhir':
                  $item_tidak_lengkap='Diagnosa Akhir';
                  break;
              case 'rek_kode_icd10':
                  $item_tidak_lengkap='Kode ICD 10';
                  break;
              case 'rek_pem_medika':
                  $item_tidak_lengkap='Pemberian Medikamentosa';
                  break;
              case 'rek_obat_pulang':
                  $item_tidak_lengkap='Pemberian Obat Waktu Pulang';
                  break;
              case 'rek_kondisi_pulang':
                  $item_tidak_lengkap='Kondisi Pulang';
                  break;
              case 'rek_instruksi':
                  $item_tidak_lengkap='Tindak Lanjut';
                  break;
              case 'rek_indikasi_pulang':
                  $item_tidak_lengkap='Indikasi Pulang';
                  break;
              case 'rek_tgl_kontrol':
                  $item_tidak_lengkap='Tanggal Kontrol';
                  break;
              case 'rek_kondisi_men':
                  $item_tidak_lengkap='Kondisi Mendesak';
                  break;
              case 'rek_transport':
                  $item_tidak_lengkap='Alat Transportasi';
                  break;
              case 'aut_nama_dokter':
                  $item_tidak_lengkap='autentifikasi Nama Dokter';
                  break;
              case 'aut_ttd_dokter':
                  $item_tidak_lengkap='autentifikasi Ttd Dokter';
                  break;
              case 'cat_anggal':
                  $item_tidak_lengkap='Pencatatan Tanggal Pelayanan';
                  break;
              case 'cat_baris':
                  $item_tidak_lengkap='Pencatatan Baris';
                  break;
              case 'cat_koreksi':
                  $item_tidak_lengkap='Pencatatan Koreksi';
                  break;
              default:
                  $item_tidak_lengkap='Tidak Ada Data Dibulan Ini';
          }
        }else{
          $chart_l_avg = 0;
          $chart_tl_avg = 0;
          $item_tidak_lengkap='Tidak Ada Data ';
          $ruang_tl = 'Tidak Ada Data';
        }

            return view('admin_assembling.dashboard.index', compact('bln','thn'))
//                ->with('bulan', json_encode($data1, JSON_NUMERIC_CHECK))
                ->with('jumlah', json_encode($data2, JSON_NUMERIC_CHECK))
                ->with('chart_l', json_encode($chart_l_avg))
                ->with('chart_tl', json_encode($chart_tl_avg))
                ->with('ruangan_tl',$ruang_tl)
                ->with('item_tl',$item_tidak_lengkap);
    }

    // public function index_dash_operator()
    // {
    //     $dt_dokter = Dokter::count('*');
    //     $dt_admin_pelapor = admin_pelapor::count('*');
    //     $dt_admin_assembling = admin_assembling::count('*');
    //     $dt_ruang = Ruang::count('*');
    //     // dd($dt_dokter);
    //
    //     return view('operator.dashboard.index', compact('dt_dokter', 'dt_admin_pelapor', 'dt_admin_assembling', 'dt_ruang'));
    // }
}
