<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dokter;
class OperatorDataDokterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data_dokter = Dokter::orderBy('updated_at','DESC')->get();

      return view ('admin_assembling.master_dokter.index',compact('data_dokter'))->with('no',1);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view ('admin_assembling.master_dokter.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $data_dokter = new Dokter;
      // $data_dokter->id_dokter = $request['dokter_sip'];
      $data_dokter->nama_dokter = $request['dokter_nama'];
      $data_dokter->jenis_kelamin = $request['dokter_jk'];
      $data_dokter->spesialis = $request['dokter_spesialis'];
      $data_dokter->save();

      return redirect('data_dokter');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data = Dokter::find($id);
      return view('admin_assembling.master_dokter.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $data_dokter = Dokter::find($id);
      $data_dokter->id_dokter = $request['dokter_sip'];
      $data_dokter->nama_dokter = $request['dokter_nama'];
      $data_dokter->jenis_kelamin = $request['dokter_jk'];
      $data_dokter->spesialis = $request['dokter_spesialis'];
      $data_dokter->update();

      return redirect('data_dokter');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $data_dokter = Dokter::find($id);
      $data_dokter->delete();

      return redirect('data_dokter');
    }

    // CEK NOMOR SIP SUDAH DI PAKAI ATAU BELUM
    public function cek_no_sip(Request $request)
    {
        if ($request->has('dokter_sip')) {
            $cari = $request->dokter_sip;
             $data = DB::table('tb_dokter')->select('dokter_sip')->where('dokter_sip',$cari )->get();

            return response()->json($data);
        }
    }
}
