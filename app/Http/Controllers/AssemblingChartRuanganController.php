<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Analisis;
use App\Ruang;
use App\View_kel_ruang;
use DB;
use App\Pasien;
use Excel;
class AssemblingChartRuanganController extends Controller
{
  public function index()
  {
      $thn = date('Y');
      $bln = date('m');
      $awal=null;
      $akhir=null;
      $nama_ruang =null;
      // $chart_ruang = Analisis::join('tb_ruangan', 'analisis_drm.id_ruang', '=', 'tb_ruangan.id_ruang')
      //     ->join('tb_pasien', 'analisis_drm.no_rm', '=', 'tb_pasien.no_rm')
      //     ->whereMonth('tgl_analisa',$bln)
      //     ->whereYear('tgl_analisa',$thn)
      //     ->groupBy('analisis_drm.no_rm')->orderBy('jml_data', 'DESC')
      //     ->select('view_hasil_kel_ruang')->get();
$chart_ruang = DB::select(DB::raw("select DISTINCT abc.nama_ruang,abc.id_ruang
,(select count(jml_data) from view_kel_ruang c where c.id_ruang=abc.id_ruang and c.jml_data=0) tl
,(select count(jml_data) from view_kel_ruang c where c.id_ruang=abc.id_ruang and c.jml_data=1) lengkap
,(select count(jml_data) from view_kel_ruang c where c.id_ruang=abc.id_ruang and c.jml_data=0) +(select count(jml_data) from view_kel_ruang c where c.id_ruang=abc.id_ruang and c.jml_data=1) as jml
from(SELECT analisis_drm.id_ruang, tb_ruangan.nama_ruang, ((CASE
                            WHEN alamat IS NULL THEN 0
                            WHEN nama IS NULL THEN 0
                            WHEN jenis_kelamin IS NULL THEN 0
                            WHEN tgl_lahir IS NULL THEN 0
                            ELSE 1 END)* (analisis_drm.rek_indikasi_masuk
	* analisis_drm.rek_diagnosa_masuk
	* analisis_drm.rek_prosedur
	* analisis_drm.rek_pem_fisik
	* analisis_drm.rek_pem_penunjang
	* analisis_drm.rek_tindakan_pros
	* analisis_drm.rek_kode_icd9
    * analisis_drm.rek_diagnosa_akhir
    * analisis_drm.rek_kode_icd10
    * analisis_drm.rek_pem_medika
    * analisis_drm.rek_obat_pulang
    * analisis_drm.rek_kondisi_pulang
	* analisis_drm.rek_instruksi
    * analisis_drm.rek_indikasi_pulang
    * analisis_drm.rek_tgl_kontrol
    * analisis_drm.rek_kondisi_men
	* analisis_drm.rek_transport
    * analisis_drm.aut_nama_dokter
    * analisis_drm.aut_ttd_dokter
    * analisis_drm.cat_tanggal
    * analisis_drm.cat_baris
    * analisis_drm.cat_koreksi)) as jml_data
    FROM `analisis_drm`
    INNER JOIN tb_ruangan on tb_ruangan.id_ruang = analisis_drm.id_ruang
    inner join `tb_pasien` on `analisis_drm`.`no_rm` = `tb_pasien`.`no_rm`
    where MONTH(analisis_drm.tgl_analisa)='$bln' AND YEAR(analisis_drm.tgl_analisa)='$thn') as abc"));


          $avgTrueArr=array();
          $avgFalseArr=array();
          // $dta_lngkap = array_sum(array_column($chart_ruang, 'lengkap'));
          // $dta_tl = array_sum(array_column($chart_ruang, 'tl'));
          // $jml_data_ruangan = array_column($chart_ruang, 'jml_data');
          // dd($pembagi);
          $data1 = array_column($chart_ruang, 'lengkap');
          $data2 = array_column($chart_ruang, 'tl');
          $pembagi = array_column($chart_ruang, 'jml');
          // dd($chart_ruang);
          $data3 = array_column($chart_ruang, 'nama_ruang');

          for ($i=0; $i < count($chart_ruang); $i++) {
            // print_r($data1[$i] );

              if ($data1[$i]==0 AND $data2[$i]==0) {
                $persenTrue = 0;
                $persenFalse = 0;
              }else{
                // print_r($data1[$i] + $data2[$i]);
                $persenTrue = round(($data1[$i]/$pembagi[$i])*100);
                $persenFalse = round(($data2[$i]/$pembagi[$i])*100);
              }
              array_push($avgTrueArr,$persenTrue);
              array_push($avgFalseArr,$persenFalse);
            }
            // dd($avgTrueArr);
            // dd($chart_ruang);
            // dd($data1[$i]);
            $dta_ruangan = Ruang::select('id_ruang','nama_ruang')->orderBy('nama_ruang','ASC')->get();
            // dd($dta_ruangan);
      return view('admin_assembling.chart_analisis.data_ruangan', compact('year','dta_ruangan','bln','thn','awal','akhir','nama_ruang'))
          //   // ->response()->json($query_lengkap);
          ->with('ruangan', json_encode($data3))
          // ->with('jmlh', json_encode($pembagi))
          ->with('jmlh', json_encode($pembagi))
          ->with('jmlh_ruang', 0)
          ->with('jmlh_pasien', 0)
          ->with('total_lengkap', json_encode($avgTrueArr, JSON_NUMERIC_CHECK))
          ->with('total_tidak_lengkap', json_encode($avgFalseArr, JSON_NUMERIC_CHECK));
  }

  public function chart_kelengkapan_ruangan_pertanggal(Request $request)
  {
      $awal = date('Y-m-d', strtotime(str_replace('/', '-', $request['cari_awal'])));
      $akhir = date('Y-m-d', strtotime(str_replace('/', '-', $request['cari_akhir'])));
      $nama_ruang =null;

      $chart_ruang = DB::select(DB::raw("select DISTINCT abc.nama_ruang,abc.id_ruang
      ,(select count(jml_data) from view_kel_ruang c where c.id_ruang=abc.id_ruang and c.jml_data=0) tl
      ,(select count(jml_data) from view_kel_ruang c where c.id_ruang=abc.id_ruang and c.jml_data=1) lengkap
      ,(select count(jml_data) from view_kel_ruang c where c.id_ruang=abc.id_ruang and c.jml_data=0) +(select count(jml_data) from view_kel_ruang c where c.id_ruang=abc.id_ruang and c.jml_data=1) as jml
      from(SELECT analisis_drm.id_ruang, tb_ruangan.nama_ruang, ((CASE
                                  WHEN alamat IS NULL THEN 0
                                  WHEN nama IS NULL THEN 0
                                  WHEN jenis_kelamin IS NULL THEN 0
                                  WHEN tgl_lahir IS NULL THEN 0
                                  ELSE 1 END)* (analisis_drm.rek_indikasi_masuk
      	* analisis_drm.rek_diagnosa_masuk
      	* analisis_drm.rek_prosedur
      	* analisis_drm.rek_pem_fisik
      	* analisis_drm.rek_pem_penunjang
      	* analisis_drm.rek_tindakan_pros
      	* analisis_drm.rek_kode_icd9
          * analisis_drm.rek_diagnosa_akhir
          * analisis_drm.rek_kode_icd10
          * analisis_drm.rek_pem_medika
          * analisis_drm.rek_obat_pulang
          * analisis_drm.rek_kondisi_pulang
      	* analisis_drm.rek_instruksi
          * analisis_drm.rek_indikasi_pulang
          * analisis_drm.rek_tgl_kontrol
          * analisis_drm.rek_kondisi_men
      	* analisis_drm.rek_transport
          * analisis_drm.aut_nama_dokter
          * analisis_drm.aut_ttd_dokter
          * analisis_drm.cat_tanggal
          * analisis_drm.cat_baris
          * analisis_drm.cat_koreksi)) as jml_data
          FROM `analisis_drm`
          INNER JOIN tb_ruangan on tb_ruangan.id_ruang = analisis_drm.id_ruang
          inner join `tb_pasien` on `analisis_drm`.`no_rm` = `tb_pasien`.`no_rm`
          where (analisis_drm.tgl_analisa >= '$awal' and analisis_drm.tgl_analisa <= '$akhir')) as abc"));
          // dd($chart_ruang);
          $avgTrueArr=array();
          $avgFalseArr=array();
          $data1 = array_column($chart_ruang, 'lengkap');
          $data2 = array_column($chart_ruang, 'tl');
          $pembagi = array_column($chart_ruang, 'jml');
          // dd($chart_ruang);
          $data3 = array_column($chart_ruang, 'nama_ruang');

          for ($i=0; $i < count($chart_ruang); $i++) {
            // print_r($data1[$i] );

              if ($data1[$i]==0 AND $data2[$i]==0) {
                $persenTrue = 0;
                $persenFalse = 0;
              }else{
                // print_r($data1[$i] + $data2[$i]);
                $persenTrue = round(($data1[$i]/$pembagi[$i])*100);
                $persenFalse = round(($data2[$i]/$pembagi[$i])*100);
              }
              array_push($avgTrueArr,$persenTrue);
              array_push($avgFalseArr,$persenFalse);
            }
            $dta_ruangan = Ruang::select('id_ruang','nama_ruang')->orderBy('nama_ruang','ASC')->get();
            // dd($avgTrueArr);
//
      return view('admin_assembling.chart_analisis.data_ruangan', compact('year','dta_ruangan','bln','thn','awal','akhir','nama_ruang'))
          //   // ->response()->json($query_lengkap);
          ->with('ruangan', json_encode($data3))
          ->with('jmlh', json_encode($pembagi))
          ->with('jmlh_ruang', 0)
          ->with('jmlh_pasien', 0)
          ->with('total_lengkap', json_encode($avgTrueArr, JSON_NUMERIC_CHECK))
          ->with('total_tidak_lengkap', json_encode($avgFalseArr, JSON_NUMERIC_CHECK));
  }

  public function chart_kelengkapan_ruangan_perruangan(Request $request)
  {
      $awal = date('Y-m-d', strtotime(str_replace('/', '-', $request['cari_awal'])));
      $akhir = date('Y-m-d', strtotime(str_replace('/', '-', $request['cari_akhir'])));
      $id_ruang = $request['ruangan_nama'];

      $pasien_l = Analisis::join('tb_pasien','tb_pasien.no_rm','=','analisis_drm.no_rm')
                                ->where('analisis_drm.id_ruang',$id_ruang)
                                ->whereNotNull('tgl_lahir')
                                ->whereNotNull('jenis_kelamin')
                                ->whereNotNull('alamat')
                                ->whereBetween('tgl_analisa',[$awal,$akhir])->count();
      $pasien_tl = Analisis::join('tb_pasien','tb_pasien.no_rm','=','analisis_drm.no_rm')
                                ->where('analisis_drm.id_ruang',$id_ruang)
                                ->whereNull('tgl_lahir')
                                ->WhereNull('jenis_kelamin')
                                ->WhereNull('alamat')
                                ->whereBetween('tgl_analisa',[$awal,$akhir])
                                ->count();
                                // dd($pasien_l);

      $records = DB::table('analisis_drm')
      ->join('tb_pasien','tb_pasien.no_rm','=','analisis_drm.no_rm')
      ->join('tb_dokter','tb_dokter.id_dokter','=','analisis_drm.id_dokter')
      ->join('tb_ruangan','tb_ruangan.id_ruang','=','analisis_drm.id_ruang')
      ->where('analisis_drm.id_ruang',$id_ruang)
      ->whereBetween('tgl_analisa',[$awal,$akhir])->get();

      $columns = ['rek_indikasi_masuk', 'rek_diagnosa_masuk',
          'rek_prosedur', 'rek_pem_fisik', 'rek_pem_penunjang', 'rek_tindakan_pros', 'rek_kode_icd9',
          'rek_diagnosa_akhir', 'rek_kode_icd10', 'rek_pem_medika', 'rek_obat_pulang', 'rek_kondisi_pulang',
          'rek_instruksi','rek_indikasi_pulang', 'rek_tgl_kontrol', 'rek_kondisi_men', 'rek_transport', 'aut_nama_dokter',
           'aut_ttd_dokter', 'cat_tanggal', 'cat_baris','cat_koreksi'];
      $data = [];
      $jumlahdata="";
      foreach ($columns as $key=> $column) {
          $data[] = [
              'nama_kolom' => $column,
              'jumlah' => $records->count(),
              // 'jumlah' => ($key == 0)? $jumlahdata=$column : $jumlahdata *= $column,
              'count_true' => $records->where($column, 1)->count(),
              'count_false' => $records->where($column, 0)->count()
          ];
      }
      if ($pasien_l == 0 AND $pasien_tl == 0) {
        $pasien_avg_l = 0;
        $pasien_avg_tl = 0;
      }else {
        $pasien_avg_l = round($pasien_l / ($pasien_l+$pasien_tl) * 100);
        $pasien_avg_tl = round($pasien_tl / ($pasien_l+$pasien_tl) * 100);
      }
      $avgTrueArr=array();
      $avgFalseArr=array();
      array_push($avgTrueArr,$pasien_avg_l);
      array_push($avgFalseArr,$pasien_avg_tl);
      $data_kolom = array_column($data, 'jumlah');
      $data_true = array_column($data, 'count_true');
      $data_false = array_column($data, 'count_false');
      $jml_data=$data_kolom[0];
      $jml_pasien = $pasien_l + $pasien_tl;
      $jml_data_pasien=$pasien_l+$pasien_tl;
      for ($i=0; $i < count($data_kolom); $i++) {
          if ($data_true[$i]==0 AND $data_false[$i]==0) {
            $persenTrue = 0;
            $persenFalse = 0;
          }else{
            $persenTrue = round(($data_true[$i]/$data_kolom[$i])*100);
            $persenFalse = round(($data_false[$i]/$data_kolom[$i])*100);
          }
          array_push($avgTrueArr,$persenTrue);
          array_push($avgFalseArr,$persenFalse);
        }
        $nama_ruang='';
        $id_ruang='';
        foreach ($records as $key => $value) {
          $nama_ruang=$value->nama_ruang;
          $id_ruang=$value->id_ruang;
        }
        // dd($records);
        $data3 =array("Ident","Ind Px", "dx Msk", "Prosedur", "Fisik", "Penunjang", "Tind Pros", "Icd 9", "Dx Akhir", "Icd 10", "Medika",
        "Obat Plg", "knd plg", "Inst",
         "Indikasi", "Tgl Ktrl", "Knd Mndsk", "Trans","Nama Dok","Ttd Dok","Baris","Korksi","tgl" );
        $dta_ruangan = Ruang::select('id_ruang','nama_ruang')->orderBy('nama_ruang','ASC')->get();
      return view('admin_assembling.chart_analisis.data_ruangan', compact('year','id_ruang','dta_ruangan','bln','thn','awal','akhir','nama_ruang'))
          //   // ->response()->json($query_lengkap);
          ->with('ruangan', json_encode($data3))
          ->with('jmlh_ruang', json_encode($jml_data))
          ->with('jmlh_pasien', json_encode($jml_pasien))
          ->with('jmlh', 1)
          ->with('total_lengkap', json_encode($avgTrueArr, JSON_NUMERIC_CHECK))
          ->with('total_tidak_lengkap', json_encode($avgFalseArr, JSON_NUMERIC_CHECK));
  }
  public function print_ruang($awal,$akhir,$id)
  {
    $bln = null;
    $thn = null;

    $dt = Analisis::select('*',DB::raw('(rek_indikasi_masuk * rek_diagnosa_masuk * rek_prosedur * rek_pem_fisik * rek_pem_penunjang * rek_tindakan_pros
                    * rek_kode_icd9 * rek_diagnosa_akhir * rek_kode_icd10 * rek_pem_medika * rek_obat_pulang * rek_kondisi_pulang
                     * rek_instruksi * rek_indikasi_pulang * rek_tgl_kontrol * rek_kondisi_men
                      * rek_transport * aut_nama_dokter * aut_ttd_dokter * cat_tanggal * cat_baris * cat_koreksi) as status')
                      )
                  ->join('tb_dokter','tb_dokter.id_dokter','=','analisis_drm.id_dokter')
                  ->join('tb_pasien','tb_pasien.no_rm','=','analisis_drm.no_rm')
                  ->join('tb_ruangan','tb_ruangan.id_ruang','=','analisis_drm.id_ruang')
                  ->where('analisis_drm.id_ruang',$id)
                  ->whereBetween('tgl_analisa',[$awal,$akhir])
                  ->get();
                  // dd($dt);
                  $nama_ruang = Ruang::where('id_ruang',$id)->value('nama_ruang');

                  // dd($dt);
        return Excel::create('Lap_Ruangan - '.$awal.$akhir, function($excel) use ($dt,$bln,$thn,$awal,$akhir,$nama_ruang) {

              $excel->sheet('Resume Medis', function($sheet) use ($dt,$bln,$thn,$awal,$akhir,$nama_ruang) {

                  $sheet->loadView('admin_assembling.chart_analisis.print_excel_ruang',compact('dt','bln','thn','awal','akhir','nama_ruang'))->with('no',1);

              });

          })->download('xls');
  }
}
