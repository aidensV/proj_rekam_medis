<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Analisis;
use DB;
use App\Pasien;
use App\Dokter;
use App\Ruang;

use App\Exports\printDetailExport;
use Excel;

class AssemblingDataDrm extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      // $data_a = DB::table('analisis_drm')
      // ->select(DB::raw('count(analis_no) as `total`'),
      //          DB::raw("DATE_FORMAT(analis_tgl, '%m-%Y') new_date"),
      //          DB::raw('YEAR(analis_tgl) year, MONTH(analis_tgl) month'))
      // ->groupby('year','month')
      // ->get();
      $thn = date('Y');
      $bln = date('m');

      $data_a = Analisis::select('no_analisa','analisis_drm.no_rm','id_ruang','id_petugas',
                  'tgl_analisa','status_pengembalian','mrs','krs','tb_pasien.nama','tb_pasien.tgl_lahir','tb_pasien.jenis_kelamin','tb_pasien.alamat',
                   DB::raw('(rek_indikasi_masuk * rek_diagnosa_masuk * rek_prosedur * rek_pem_fisik * rek_pem_penunjang * rek_tindakan_pros
                    * rek_kode_icd9 * rek_diagnosa_akhir * rek_kode_icd10 * rek_pem_medika * rek_obat_pulang * rek_kondisi_pulang
                     * rek_instruksi * rek_indikasi_pulang * rek_tgl_kontrol * rek_kondisi_men
                      * rek_transport * aut_nama_dokter * aut_ttd_dokter * cat_tanggal * cat_baris * cat_koreksi) as status')
                      )
                       ->join('tb_pasien','tb_pasien.no_rm','=','analisis_drm.no_rm')
                       ->whereMonth('tgl_analisa', $bln)
                       ->whereYear('tgl_analisa', $thn)
                       ->orderBy('analisis_drm.updated_at','DESC')
                       ->get();
// dd($data_a);
      return view('admin_assembling.data_analisis.index',compact('data_a'))->with('no',1);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $data_detail = Analisis::select('no_analisa','analisis_drm.no_rm','id_ruang','id_petugas',
                  'tgl_analisa','status_pengembalian','mrs','krs','tb_pasien.nama','tb_pasien.tgl_lahir','tb_pasien.jenis_kelamin','tb_pasien.alamat',
                   DB::raw('(rek_indikasi_masuk * rek_diagnosa_masuk * rek_prosedur * rek_pem_fisik * rek_pem_penunjang * rek_tindakan_pros
                    * rek_kode_icd9 * rek_diagnosa_akhir * rek_kode_icd10 * rek_pem_medika * rek_obat_pulang * rek_kondisi_pulang
                     * rek_instruksi * rek_indikasi_pulang * rek_tgl_kontrol * rek_kondisi_men
                      * rek_transport * aut_nama_dokter * aut_ttd_dokter * cat_tanggal * cat_baris * cat_koreksi) as status')
                      )
                        ->where('analisis_drm.no_rm',$id)
                        ->join('tb_pasien','tb_pasien.no_rm','=','analisis_drm.no_rm')
                        ->orderBy('analisis_drm.created_at','ASC')->get();
                        // dd($data_detail);

      return view('admin_assembling.data_analisis.detail_data',compact('data_detail'))->with('no',1);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data_detail_join = Analisis::select('analisis_drm.id_ruang','nama_ruang','analisis_drm.id_dokter','nama_dokter')
        ->join('tb_ruangan','tb_ruangan.id_ruang','=','analisis_drm.id_ruang')
        ->join('tb_dokter','tb_dokter.id_dokter','=','analisis_drm.id_dokter')->where('no_analisa',$id)->get();

        $data_detail=Analisis::join('tb_pasien','tb_pasien.no_rm','=','analisis_drm.no_rm')->find($id);
        // dd($data_detail);
        $dokter = Dokter::select('id_dokter','nama_dokter')->orderBy('nama_dokter','ASC')->get();
        $ruangan = Ruang::select('id_ruang','nama_ruang')->orderBy('nama_ruang','ASC')->get();

// dd($data_detail_join);
        return view('admin_assembling.data_analisis.edit',compact('data_detail','dokter','ruangan','data_detail_join'));
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      // $selectMax = Analisis::select('analis_no')->max('analis_no');
      // $getID = (int)substr($selectMax, 2, 4);
      // $char = "AN";
      //
      // $setID = $char . sprintf("%04s", $getID);
      // $setID++;


      $masuk = date('Y-m-d', strtotime(str_replace('/', '-', $request['analisis_mrs'])));
      $keluar = date('Y-m-d', strtotime(str_replace('/', '-', $request['analisis_krs'])));
      // $countDate = date_diff(date_create($keluar), date_create($masuk));
      // $formatedToInt = $countDate->format("%d");

      $analisis = Analisis::find($id);
      $analisis->no_rm = $request['pasien_no_rm'];
      $analisis->id_ruang = $request['ruang_id'];
      $analisis->id_dokter = $request['dokter_id'];

      $analisis->rek_indikasi_masuk = $request->input('cb_indi_masuk') ?? 0;
      $analisis->rek_diagnosa_masuk = $request->input('cb_diag_masuk') ?? 0;
      $analisis->rek_prosedur = $request['cb_tindakan_pros'] ?? 0;
      $analisis->rek_pem_fisik = $request['cb_pem_fisik'] ?? 0;
      $analisis->rek_pem_penunjang = $request['cb_pem_penunjang'] ?? 0;
      $analisis->rek_tindakan_pros = $request['cb_tindakan_pros'] ?? 0;
      $analisis->rek_kode_icd9 = $request['cb_icd9'] ?? 0;
      $analisis->rek_diagnosa_akhir = $request['cb_diag_akhir'] ?? 0;
      $analisis->rek_kode_icd10 = $request['cb_icd10'] ?? 0;
      $analisis->rek_pem_medika = $request['cb_pem_medik'] ?? 0;
      $analisis->rek_obat_pulang = $request['cb_obt_pulang'] ?? 0;
      $analisis->rek_kondisi_pulang = $request['cb_kond_pulang'] ?? 0;
      $analisis->rek_instruksi = $request['cb_instruksi'] ?? 0;
      $analisis->rek_indikasi_pulang = $request['cb_indi_pulang'] ?? 0;
      $analisis->rek_tgl_kontrol = $request['cb_tgl_kontrol'] ?? 0;
      $analisis->rek_kondisi_men = $request['cb_kond_mendesak'] ?? 0;
      $analisis->rek_transport = $request['cb_alat_transp'] ?? 0;

      $analisis->aut_nama_dokter = $request['cb_nama_dokter'] ?? 0;
      $analisis->aut_ttd_dokter = $request['cb_ttd_dokter'] ?? 0;

      $analisis->cat_tanggal = $request['cb_tgl'] ?? 0;
      $analisis->cat_baris = $request['cb_baris'] ?? 0;
      $analisis->cat_koreksi = $request['cb_koreksi'] ?? 0;
      $analisis->update();

      return redirect('data_analisis_drm')->with('message','Berhasil Mengubah Data');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data_detail =Analisis::find($id);
        $data_detail->delete();

        return back()->with('message','Berhasil Menghapus');
    }
    public function find_no_rm_edit(Request $request)
    {
      if ($request->has('q')) {
            $cari = $request->q;
            $data = DB::table('tb_pasien')->orWhere(DB::raw("CONCAT(`pasien_no_rm`, ' ', `pasien_nama`)"), 'LIKE', "%".$cari."%")
            // $data = DB::table('_pasien')->orWhereRaw(DB::raw("CONCAT(pasien_no_rm,'',pasien_nama)"),'LIKE',"%{$cari}%")
                    ->get();
            // $data = DB::table('tb_pasien')->select('*')->where('pasien_no_rm', 'LIKE', '%{$cari}%')->get();
            return response()->json($data);
            // return $cari;
        }
   }
    // public function detail_drm($bln,$thn )
    // {
    //   $data_detail = Analisis::join('tb_dokter','tb_dokter.dokter_id','=','analisis_drm.dokter_id')
    //                ->join('tb_pasien','tb_pasien.pasien_no_rm','=','analisis_drm.pasien_no_rm')
    //                ->whereMonth('analis_tgl',$bln)
    //                ->whereYear('analis_tgl',$thn)
    //                ->get();
    //
    //                // dd($data_a);
    //   return view('admin_assembling.data_analisis.detail_data',compact('data_detail','bln','thn'))->with('no',1);
    //
    // }
    public function print_detail_drm($bln,$thn)
    {
$awal = null;
$akhir = null;
$dt = Analisis::join('tb_dokter','tb_dokter.dokter_id','=','analisis_drm.dokter_id')
              ->join('tb_pasien','tb_pasien.pasien_no_rm','=','analisis_drm.pasien_no_rm')
              ->join('tb_ruangan','tb_ruangan.ruangan_id','=','analisis_drm.ruang_id')
              ->whereMonth('analis_tgl',$bln)
              ->whereYear('analis_tgl',$thn)
              ->get();

    return Excel::create('Lap_analisis - '.$bln.$thn, function($excel) use ($dt,$bln,$thn,$awal,$akhir) {

          $excel->sheet('Resume Medis', function($sheet) use ($dt,$bln,$thn,$awal,$akhir) {

              $sheet->loadView('admin_assembling.data_analisis.print_excel_detail',compact('dt','bln','thn','awal','akhir'))->with('no',1);

          });

      })->download('xls');
    }

    public function print_detail_drm_cari($awal,$akhir)
    {


$dt = Analisis::join('tb_dokter','tb_dokter.dokter_id','=','analisis_drm.dokter_id')
              ->join('tb_pasien','tb_pasien.pasien_no_rm','=','analisis_drm.pasien_no_rm')
              ->join('tb_ruangan','tb_ruangan.ruangan_id','=','analisis_drm.ruang_id')
              ->whereBetween('analis_tgl',[$awal,$akhir])
              ->get();
              // dd($dt);
    return Excel::create('Lap_analisis - '.$awal.$akhir, function($excel) use ($dt,$awal,$akhir) {

          $excel->sheet('Resume Medis', function($sheet) use ($dt,$awal,$akhir) {

              $sheet->loadView('admin_assembling.data_analisis.print_excel_detail',compact('dt','awal','akhir'))->with('no',1);

          });

      })->download('xls');
    }
    public function index_data_analis_persen($bln,$thn)
    {

      $thn= $thn;
      $bln= $bln;

      $year = DB::table('analisis_drm')->select(DB::raw('YEAR(analis_tgl) AS thn'))->distinct()->get();
      $records = DB::table('analisis_drm')
      ->whereMonth('analis_tgl',$bln)
      ->whereYear('analis_tgl',$thn)->get();
      $columns = ['rek_indikasi_masuk', 'rek_diagnosa_masuk', 'rek_tindakan',
      'rek_pem_fisik','rek_pem_penunjang','rek_hasil_tindakan','rek_kode_icd9','rek_kode_icd10',
      'rek_diagnosa_akhir','rek_pem_medik','rek_obat_pulang','rek_kondisi_pulang','rek_tindak_lanjut',
      'rek_indikasi_pulang','rek_tgl_kontrol','rek_kondisi_mendesak','rek_alat_transportasi','aut_nama_dokter','aut_ttd_dokter','cat_koreksi','cat_baris'];
      $data = [];
      foreach($columns as $column) {
          $data[] = [

              'kolom' => $column,
              'count_true' => $records->where($column, 1)->count(),
              'count_kolom' => $records->count(),
              'count_false' => $records->where($column, 0)->count()
          ];
        }
 // Persen Untuk Centang Lengkap
        $avgTrueArr = array();
        $data1 = array_column($data,'count_kolom');
        $data2 = array_column($data,'count_true');
        for ($i=0; $i < count($data1); $i++) {
          if ($data2[$i]==0) {
            $persenTrue = 0;
          }
          else {
            $persenTrue = ($data2[$i]/$data1[$i])*100;
          }
          array_push($avgTrueArr,$persenTrue);
        }
 // Persen Untuk centan Tidak Lengkap
        $avgFalseArr = array();

        $data3 = array_column($data,'count_kolom');
        $data4 = array_column($data,'count_false');
        for ($y=0; $y < count($data3); $y++) {
          if ($data4[$y]==0) {
            $persenFalse = 0;
          }
          else {
            $persenFalse = ($data4[$y]/$data3[$y])*100;
          }
          array_push($avgFalseArr,$persenFalse);
        }

      return view('admin_assembling.persent_analisis.index',compact('year','data','avgTrueArr','avgFalseArr'));

    }
}
