<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Pasien;
use App\Dokter;
use App\Ruang;
use App\Analisis;
use Auth;
use PDF;
use App;

class AssemblingAnalisisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      // GET DATA DARI TABEL
        $dokter = Dokter::select('id_dokter', 'nama_dokter')->orderBy('nama_dokter', 'ASC')->get();
        $ruangan = Ruang::select('id_ruang', 'nama_ruang')->orderBy('nama_ruang', 'ASC')->get();

        return view('admin_assembling.analisis.index', compact('dokter', 'ruangan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

// MENIMPAN DATA DATA CENTANG
        // $selectRM = Analisis::where('pasien_no_rm',$request['pasien_no_rm'])->min('analis_id');
        //   if ($selectRM) {
        //   $update_status = Analisis::where('analis_id',$selectRM)->first();
        //   // dd($selectRM);
        //   $update_status->analis_status_peng = '4';
        //   $update_status->update();
        //   }

        // $selectMax = Analisis::select('analis_no')->max('analis_no');
        // $getID = (int)substr($selectMax, 2, 4);
        // $char = "AN";
        //
        // $setID = $char . sprintf("%04s", $getID);
        // $setID++;

        $id_petugas=Auth::user()->id_petugas;
        $masuk = date('Y-m-d', strtotime(str_replace('/', '-', $request['analisis_mrs'])));
        $keluar = date('Y-m-d', strtotime(str_replace('/', '-', $request['analisis_krs'])));
        $countDate = date_diff(date_create($keluar), date_create($masuk));
        $formatedToInt = $countDate->format("%d");

        $analisis = new Analisis;
        $analisis->mrs = date('Y-m-d', strtotime(str_replace('/', '-', $request['analisis_mrs'])));
        $analisis->krs = date('Y-m-d 23:59:59', strtotime(str_replace('/', '-', $request['analisis_krs'])));
        $tgl_kembali= date('Y-m-d 23:59:59', strtotime(str_replace('/', '-', $request['tgl_cocok'])));
        $tgl_sekarang= date('Y-m-d 23:59:59');
        // dd($tgl);
        // if ($tgl_sekarang >= $tgl_kembali) {
        //   $analisis->status_pengembalian = '3';
        //
        // }else {
        //   $analisis->status_pengembalian = '2';
        // }
        $analisis->no_rm = $request['pasien_no_rm'];
        $analisis->id_ruang = $request['ruang_id'];
        $analisis->id_dokter = $request['dokter_id'];
        $analisis->tgl_analisa = date('Y-m-d');
        $analisis->id_petugas = $id_petugas;

        $analisis->rek_indikasi_masuk = $request->input('cb_indi_masuk') ?? 0;
        $analisis->rek_diagnosa_masuk = $request->input('cb_diag_masuk') ?? 0;
        $analisis->rek_prosedur = $request['cb_tindakan_pros'] ?? 0;
        $analisis->rek_pem_fisik = $request['cb_pem_fisik'] ?? 0;
        $analisis->rek_pem_penunjang = $request['cb_pem_penunjang'] ?? 0;
        $analisis->rek_tindakan_pros = $request['cb_tindakan_pros'] ?? 0;
        $analisis->rek_kode_icd9 = $request['cb_icd9'] ?? 0;
        $analisis->rek_diagnosa_akhir = $request['cb_diag_akhir'] ?? 0;
        $analisis->rek_kode_icd10 = $request['cb_icd10'] ?? 0;
        $analisis->rek_pem_medika = $request['cb_pem_medik'] ?? 0;
        $analisis->rek_obat_pulang = $request['cb_obt_pulang'] ?? 0;
        $analisis->rek_kondisi_pulang = $request['cb_kond_pulang'] ?? 0;
        $analisis->rek_instruksi = $request['cb_instruksi'] ?? 0;
        $analisis->rek_indikasi_pulang = $request['cb_indi_pulang'] ?? 0;
        $analisis->rek_tgl_kontrol = $request['cb_tgl_kontrol'] ?? 0;
        $analisis->rek_kondisi_men = $request['cb_kond_mendesak'] ?? 0;
        $analisis->rek_transport = $request['cb_alat_transp'] ?? 0;

        $analisis->aut_nama_dokter = $request['cb_nama_dokter'] ?? 0;
        $analisis->aut_ttd_dokter = $request['cb_ttd_dokter'] ?? 0;

        $analisis->cat_tanggal = $request['cb_tgl'] ?? 0;
        $analisis->cat_baris = $request['cb_baris'] ?? 0;
        $analisis->cat_koreksi = $request['cb_koreksi'] ?? 0;
        $analisis->save();

        return back();

    }

    // /**
    //  * Display the specified resource.
    //  *
    //  * @param  int $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function show($id)
    // {
    //     //
    // }
    //
    // /**
    //  * Show the form for editing the specified resource.
    //  *
    //  * @param  int $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function edit($id)
    // {
    //     //
    // }
    //
    // /**
    //  * Update the specified resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request $request
    //  * @param  int $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function update(Request $request, $id)
    // {
    //     //
    // }
    //
    // /**
    //  * Remove the specified resource from storage.
    //  *
    //  * @param  int $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function destroy($id)
    // {
    //     //
    // }

    // MENCARI NO RM SAAT DI KETIK DI FORM REVIEW_iDENTIFIKASI
    public function find_no_rm(Request $request)
    {
        if ($request->has('q')) {
            $cari = $request->q;
           $data = Pasien::orWhere(DB::raw("CONCAT(no_rm, ' ', nama)"), 'LIKE', "%" . $cari . "%")
               // $data = DB::table('_pasien')->orWhereRaw(DB::raw("CONCAT(pasien_no_rm,'',pasien_nama)"),'LIKE',"%{$cari}%")
               ->get();
            return response()->json($data);
        }
    }
    // MENGAMBIL DATA DARI DATABESE UNTUK DI TAMPILKAN
    public function get_no_rm(Request $request)
    {
        if ($request->has('no_rm')) {
            $cari = $request->no_rm;
//            $data = DB::table('tb_pasien')->orWhere(DB::raw("CONCAT(pasien_no_rm, ' ', pasien_nama)"), 'LIKE', "%" . $cari . "%")
//                // $data = DB::table('_pasien')->orWhereRaw(DB::raw("CONCAT(pasien_no_rm,'',pasien_nama)"),'LIKE',"%{$cari}%")
//                ->get();
             $data = DB::table('tb_pasien')->select('*')->where('no_rm',$cari )->first();

            return response()->json($data);
        }
    }
    public function cek_krs(Request $request)
    {
        if ($request->has('no_rm')) {
            $cari = $request->no_rm;
             $data = DB::table('analisis_drm')->select('krs','no_rm')->where('no_rm',$cari )->orderBy('krs','DESC')->first();

            return response()->json($data);
        }
    }










}
